##### Financial Libraries

Backtesting: https://pmorissette.github.io/bt/
Portfolio Optimisation, Black Litterman: https://pyportfolioopt.readthedocs.io/en/latest/ExpectedReturns.html
Technical Indicators: https://github.com/mrjbq7/ta-lib and manually coded indicators

##### Strategy:

Trend Indicator: EMA MACD
Momentum Indicator: ADX, RSI probability score of pulling back
Volatility Indicator: Bollinger Bands, Relatively high price closer to the upper band, low price to the lower band. Use 1 SD.
