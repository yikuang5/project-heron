from datetime import datetime, timedelta
from dotenv import load_dotenv
from bs4 import BeautifulSoup
import pandas as pd
import requests
import time
import ast
import os
load_dotenv()
# Global API Keys
coinapi = os.environ["COIN_API_KEY"]
finnhubapi = os.environ["FINHUB_API_KEY"]


def HistoricalData(symbol, financial_instrument: str = "", date: str = "", resolution: int = 30):
    """ This function doubles as allocation to spread out the rate limits and to stream the right historical data

    Default entry is to past 1 month data, otherwise, use custom date. 

    Parameters
    ----------------------------------------------------------------------
    symbol                : Uppercase string
    financial_instrument  : Stock or Cryptocurrency
    date                  : %Y-%m-%d

    """

    if date == "":
        date = datetime.strftime(
            datetime.now() - timedelta(days=31), "%Y-%m-%d")

    if financial_instrument == "":
        raise ValueError(
            "No Financial Instrument Entered. Enter Financial Instrument Type of either 'Stock' or 'Cryptocurrency'")

    if financial_instrument == "Stock":
        q = HistoricalData_FINNHUBAPI(
            ticker=symbol, resolution=resolution, from_date=date, data_format="csv")
        return q
    elif financial_instrument == "Cryptocurrency":
        q = CryptoHistoricalData_COINAPI(symbol=symbol, from_date=date)
        return q


def UNIX(date, datetime_format):
    """ Return UNIX Time Stamp give a date and datetime format
    """

    # %Y-%m-%d
    d = datetime.strptime(date, datetime_format)
    unixtime = time.mktime(d.timetuple())
    return int(unixtime)


def RetrieveSymbols_FINNHUBAPI():
    """ Get all the available Forex and Stock Symbols available on FinnHub API
    """

    forexSymbols = requests.get(
        "https://finnhub.io/api/v1/forex/symbol?exchange=oanda&token=c4uvoriad3id268asmjg").text
    stockSymbols = requests.get(
        "https://finnhub.io/api/v1/stock/symbol?exchange=US&token=c4uvoriad3id268asmjg").text

    supportStocks = pd.DataFrame(eval(stockSymbols))
    supportForex = pd.DataFrame(eval(forexSymbols))

    supportForex['type'] = "Forex"
    supportStocks = supportStocks.rename(
        columns={"currency": "Currency", "figi": "FIGI", "mic": "MIC"})
    symbols = pd.concat([supportStocks, supportForex])

    return symbols


def HistoricalData_FINNHUBAPI(ticker: str, from_date: str, resolution: int = 1, data_format: str = "json"):
    """

    Parameters
    ----------------------------------------------------------------------
    from_date: %Y-%m-%d
    resolution: Supported resolution includes 1, 5, 15, 30, 60, D, W, M .Some timeframes might not be available depending on the exchange.

    Rate Limit 
    ======================
    60 API calls/minute	

    """

    to_date = datetime.strftime(datetime.now() + timedelta(days=1), "%Y-%m-%d")
    fromdate, todate = UNIX(from_date, "%Y-%m-%d"), UNIX(to_date, "%Y-%m-%d")

    hist = requests.get(
        f"https://finnhub.io/api/v1/stock/candle?symbol={ticker}&resolution={resolution}&from={fromdate}&to={todate}&token={finnhubapi}").text
    if data_format == "json":
        return hist

    elif data_format == "csv":
        historical = pd.DataFrame(eval(hist))
        historical.columns = ['close', 'high', 'low',
                              'open', 'status', 'date', 'volume']
        historical.date = historical.date.apply(
            lambda ts: datetime.utcfromtimestamp(ts).strftime('%Y-%m-%d %H:%M:%S'))

        historical['symbol'] = ticker
        return historical


""" Coin API """


def CryptoExchangeSymbols_COINAPI(symbol: str = "OKEX"):
    """
    Get the symbols available for the Crypto Exchange. In this case its default for OKEX

    Rate Limit 
    ======================
    3 requests per second, up to 6 requests per second in bursts

    """

    url = f'https://rest.coinapi.io/v1/symbols?filter_exchange_id={symbol}'
    headers = {'X-CoinAPI-Key': coinapi}
    response = requests.get(url, headers=headers)

    symbols = eval(response.text.replace(
        "true,", "'',").replace("false,", "'',"))
    symbols = pd.DataFrame(symbols)

    spot_symbols = symbols[(symbols.symbol_type == "SPOT")
                           & (symbols.asset_id_quote == "USDT")]

    return spot_symbols


def CryptoHistoricalData_COINAPI(symbol: str = "BTC", period="30MIN", from_date="2021-09-01", limit=100000):
    """

    Period Availability
    ----------------------------------------------------------------------
    Second	1SEC, 2SEC, 3SEC, 4SEC, 5SEC, 6SEC, 10SEC, 15SEC, 20SEC, 30SEC
    Minute	1MIN, 2MIN, 3MIN, 4MIN, 5MIN, 6MIN, 10MIN, 15MIN, 20MIN, 30MIN
    Hour	1HRS, 2HRS, 3HRS, 4HRS, 6HRS, 8HRS, 12HRS
    Day	    1DAY, 2DAY, 3DAY, 5DAY, 7DAY, 10DAY
    Month	1MTH, 2MTH, 3MTH, 4MTH, 6MTH
    Year	1YRS, 2YRS, 3YRS, 4YRS, 5YRS

    Limit
    ----------------------------------------------------------------------
    Amount of items to return (optional, mininum is 1, maximum is 100000, default value is 100
    If the parameter is used then every 100 output items are counted as one request)

    Rate Limit 
    ======================
    3 requests per second, up to 6 requests per second in bursts

    Symbols Available
    ======================
   'GAS', 'NEO', 'ETH', 'BCH', 'QTUM', 'ETC', 'LTC', 'BTC', 'MKR',
   'DNA', 'PPT', 'VIB', 'UBTC', 'MDT', 'MCO', 'MDA', 'ARK', 'BNT',
   'NULS', 'AST', 'TRUE', 'THETA', 'XEM', 'TCT', 'ZRX', 'XMR', 'CVC',
   'LINK', 'PAY', 'WTC', 'ZEC', 'ICX', 'NAS', 'YOYOW', 'YEE', 'OMG',
   'KNC', 'FAIR', 'STORJ', 'XLM', 'ELF', 'MOF', 'DGB', 'ITC', 'UTK',
   'TOPC', 'RNT', 'COMET', 'DASH', 'KCASH', 'BTG', 'INT', 'SNT',
   'LRC', 'IOTA', 'MANA', 'TRX', 'PST', 'SWFTC', 'XUC', 'AAC', 'BTM',
   'ACT', 'IOST', 'BCD', 'EOS', 'XRP', 'SOC', 'ZEN', 'NANO', 'GTO',
   'CHAT', 'MITH', 'ABT', 'TRIO', 'TRA', 'REN', 'QUN', 'ENJ', 'ONT',
   'OKB', 'CTXC', 'ZIL', 'YOU', 'LSK', 'LBA', 'AE', 'SC', 'KAN',
   'DCR', 'WAVES', 'ORST', 'CVT', 'EGT', 'LET', 'ADA', 'HYC', 'USDC',
   'PAX', 'GUSD', 'TUSD', 'GNT', 'HMCN', 'BCHSV', 'LEND', 'BTT',
   'ZIP', 'BEC', 'BLOC', 'ATOM', 'EDO', 'ALV', 'LAMB', 'ETM', 'LEO',
   'ALGO', 'CRO', 'WXT', 'FTM', 'DGD', 'STC', 'DOGE', 'ORBS', 'FSN',
   'EM', 'EC', 'RFR', 'R', 'BKX', 'PLG', 'VSYS', 'SHOW', 'PRA', 'XPO',
   'INSUR', 'HBAR', 'SSC', 'LIGHT', 'OF', 'UGC', 'IPC', 'CIC', 'INS',
   'DPY', 'HMC', 'MOT', 'XTZ', 'ROAD', 'DAI', 'XAS', 'HPB', 'CAI',
   'RVN', 'WIN', 'APM', 'HDAO', 'MVP', 'BAT', 'DADI', 'OXT', 'DEP',
   'CTC', 'NDN', 'BHP', 'WGRT', 'IQ', 'TMTG', 'COMP', 'APIX', 'DMG',
   'CELO', 'DOT', 'AERGO', 'SNX', 'BAL', 'XSR', 'ANT', 'CRV', 'SRM',
   'XPR', 'DIA', 'PNK', 'OM', 'YFI', 'JST', 'TRB', 'RSR', 'BAND',
   'WNXM', 'YFII', 'UMA', 'SUSHI', 'CVP', 'KSM', 'TRADE', 'JFI',
   'TAI', 'SWRV', 'KLAY', 'SUN', 'REP', 'NMR', 'MLN', 'ZYRO', 'WBTC',
   'FRONT', 'WING', 'RIO', 'UNI', 'DHT', 'CNTM', 'AVAX', 'INX', 'DMD',
   'BOX', 'MXT', 'ANW', 'FLM', 'RFUEL', 'SFG', 'EGLD', 'SOL', 'GHST',
   'MEME', 'HSR', 'EXE', 'POE', 'OST', 'REQ', 'AAVE', 'SNC', 'RCN',
   'EVX', 'HOPL', 'LMCH', 'AIDOC', 'ENG', 'ACE', 'CAN', 'SALT', 'FUN',
   'SNGLS', 'RDN', 'TNB', 'UCT', 'AMM', 'VALUE', 'SPF', 'CAG', 'LEV',
   'OAX', 'QVT', 'ICN', 'DNT', 'KEY', 'SAN', 'DAT', '1ST', 'GNX',
   'WRC', 'MAG', 'DENT', 'GSC', 'NEAR', 'FIL', 'OK06ETT', 'KP3R',
   'PICKLE', 'COVER', 'API3', 'HEGIC', 'VIU', 'NU', 'LOON', 'VNT',
   'MTH', 'BRD', 'SUB', 'METAL', 'NGC', 'UKG', 'ATL', 'REF', 'GRT',
   'AVT', 'VEE', 'RCT', 'LON', 'BETH', '1INCH', 'CBT', 'BADGER',
   'BCHA', 'OKT', 'AUTO', 'PHA', 'POLS', 'MXC', 'PROPS', 'PRQ', 'MIR',
   'GLM', 'TORN', 'FLOW', 'TIO', 'LUNA', 'MASK', 'SNM', 'LA', 'READ',
   'CFX', 'CHZ', 'ALPHA', 'STX', 'VELO', 'PERP', 'WFEE', 'KINE',
   'BOT', 'ANC', 'SAND', 'DORA', 'CONV', 'CELR', 'MATIC', 'SKL',
   'ZKS', 'LPT', 'KONO', 'AUCTION', 'GAL', 'CEL', 'DAO', 'GTC',
   'FORTH', 'EDGE', 'VRA', 'XCH', 'CSPR', 'SHIB', 'ICP', 'LAT',
   'STRK', 'KISHU', 'AKITA', 'MINA', 'BCN', 'BZZ', 'CQT', 'SMT',
   'FEG', 'CFG', 'CTR', 'XEC', 'KAR', 'AXS', 'YFV', 'YGG', 'CLV',
   'OMI', 'EFI', 'BABYDOGE', 'WNCG', 'SLP', 'ERN', 'REVV', 'USDK',
   'BCHABC', 'AGLD', 'ILV', 'DYDX', 'CGS', 'EDEN', 'MON', 'GALA',
   'CHE'

    """
    timestart = from_date + "T00:00:00"
    ticker = "OKEX_SPOT_" + symbol + "_USDT"

    url = f'https://rest.coinapi.io/v1/ohlcv/{ticker}/history?period_id={period}&time_start={timestart}&limit={limit}'
    headers = {'X-CoinAPI-Key': coinapi}
    response = requests.get(url, headers=headers)

    data = pd.DataFrame(eval(response.text)).drop(columns=['time_period_start', 'time_open', 'time_close']).rename(columns={
        "time_period_end": "date",
        "price_open": "open",
        "price_close": "close",
        "price_low": "low",
        "price_high": "high",
        "volume_traded": "volume",
        "trade_count": "trades"})
    data.date = data.date.apply(lambda x: re.findall(
        "....-..-..T..:..:..", x)[0].replace("T", " "))

    return data


""" Gemini API """


def CryptoHistoricalData_GEMINIAPI(symbol: str = "btcusd"):
    """
    Retrieve Past 1 Month Data from Gemini, with 30 minutes time interval

    Rate Limit 
    ======================
    120 requests per minute 

    Symbols Available
    ======================
     'btcusd', 'ethbtc',
     'ethusd', 'zecusd', 'zecbtc', 'zeceth', 'zecbch', 'zecltc', 'bchusd', 'bchbtc',
     'bcheth', 'ltcusd', 'ltcbtc', 'ltceth', 'ltcbch', 'batusd', 'daiusd', 'linkusd',
     'oxtusd', 'batbtc', 'linkbtc', 'oxtbtc', 'bateth', 'linketh', 'oxteth','ampusd',
     'compusd', 'paxgusd', 'mkrusd', 'zrxusd', 'kncusd', 'manausd', 'storjusd', 'snxusd', 
     'crvusd', 'balusd', 'uniusd', 'renusd', 'umausd', 'yfiusd', 'btcdai', 'ethdai',
     'aaveusd', 'filusd', 'btceur', 'btcgbp', 'etheur', 'ethgbp', 'btcsgd', 'ethsgd',
     'sklusd', 'grtusd', 'bntusd', '1inchusd', 'enjusd', 'lrcusd', 'sandusd',
     'cubeusd', 'lptusd', 'bondusd', 'maticusd', 'injusd', 'sushiusd',
     'dogeusd', 'alcxusd', 'mirusd', 'ftmusd', 'ankrusd', 'btcgusd', 'ethgusd',
     'ctxusd', 'xtzusd', 'axsusd', 'slpusd', 'lunausd', 'ustusd', 'mco2usd'

    """

    cryptoitem = requests.get(
        f"https://api.gemini.com/v2/candles/{symbol}/30m").text
    cryptosymbol = pd.DataFrame(ast.literal_eval(cryptoitem), columns=[
                                'time', 'open', 'high', 'low', 'close', 'volume'])
    cryptosymbol.time = cryptosymbol.time.apply(
        lambda x: datetime.fromtimestamp(x/1000.0))

    return cryptosymbol


def stringToLargeLiteral(strnum):
    """ Clean M, B, T from string and convert to large integer
    """

    if "M" in strnum:

        return int(float(strnum.replace("M", "")) * 1000000)

    elif "B" in strnum:

        return int(float(strnum.replace("B", "")) * 1000000000)

    elif "T" in strnum:

        return int(float(strnum.replace("T", "")) * 1000000000)


def MarketCapitalisation_YCHARTSAPI(ticker):
    """ Get the market capitalisation based on the stock ticker
    ticker     : str
    """

    def checkLargeNumber(num):

        try:

            if float(num) > 50000:

                return True

            else:

                return False

        except Exception as e:

            return False

    url = f'https://ycharts.com/companies/{ticker}/market_cap'
    headers = {
        'User-Agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_11_5) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/50.0.2661.102 Safari/537.36'}
    res = requests.get(url, headers=headers)
    soup = BeautifulSoup(res.text, features="lxml")

    data = []

    rows = soup.find_all('tr')

    for row in rows:

        cols = row.find_all('td')

        cols = [ele.text.strip() for ele in cols]

        try:

            if str(datetime.now().year) in cols[0]:

                q = []

                for ele in cols:

                    if any(ext in ele for ext in ["M", "B", "T"]) or checkLargeNumber(ele):

                        q.append(ele)

                data.append(q)  # Get rid of empty values

        except:

            pass

    return data
