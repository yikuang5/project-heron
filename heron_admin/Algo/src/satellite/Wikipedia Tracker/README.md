### Wikipedia Tracker for Crypto and Institutions

#### Methodology

We scrape the wikipedia links of all of the companies listed on the NYSE and NASDAQ. For each of these pages scraped, we crawl the hyperlinks present in each one of them, and conduct an iteration of network analysis to single out shared nodes (either companies or technologies) and the clusters that each one of these companies share. From here, we get iter (2) of web links, where iter (1) are the initial company hyperlinks. \

Using MediaWiki API, we extract the past 30 days page views for each company (totals 1.3s on average using asynchronous requests) and conduct trend detection on each time series.

#### Methodology for short term trend predictions

#### Methodology for long term trend predictions

As a successful implementation of trend detection, we base our testing of different methodologies on the minimisation of Type I Errors and the ability to properly single out useful trends.
Our trend detection program implements three different algorithms and is derived after repeated benchmarking against a Linear Regression, one of the simplest form of trend estimation.
This algorithm, which is specific only to short time series (wiki trend page views), is based on [Comparison of Trend Detection Methods](https://scholarworks.umt.edu/cgi/viewcontent.cgi?article=1247&context=etd) (K. Gray, 2007), which implements the sum of first derivatives on the smoothed P-spline regressed time series data.

**Other Resources**
Optimizing Knot Placement in Regression Splines (Joseph Holey, 2019 - [Lancaster](https://www.lancaster.ac.uk/media/lancaster-university/content-assets/documents/stor-i/interns-docs/2019/Joe_Holey_STOR_i_Presentation.pdf))
Introduction to Regression Splines in Python ([AnalyticsVidhya](https://www.analyticsvidhya.com/blog/2018/03/introduction-regression-splines-python-codes/))

#### Steps

1. Select the location of knots based on genetic algorithm (GA).
2. Create the P-Spline Regression.
