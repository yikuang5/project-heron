from google.oauth2 import service_account
from google_auth_oauthlib.flow import InstalledAppFlow
from googleapiclient.discovery import build

import pandas as pd

from dotenv import load_dotenv
import os

load_dotenv()

credentials_path = os.environ['GOOGLE_APPLICATION_CREDENTIALS']

def _create_gsheet_object(path_to_credentials, scopes):
    
    credentials = service_account.Credentials.from_service_account_file(path_to_credentials, scopes=scopes)
    
    service = build('sheets', 'v4', credentials=credentials)
    
    sheet = service.spreadsheets()
    
    return sheet

def read_df_from_gsheets(spreadsheet_id, path_to_credentials = credentials_path, sheet_name = 'Sheet1', sheet_range = None, first_row_is_header = True):
    
    '''Returns a Pandas DataFrame from Google Sheets, 
    for a given spreadsheet_id and range. Sheet must be shared 
    with the service account, e.g. :
    googlesheet@root-sanctuary-178203.iam.gserviceaccount.com'''
    
    SCOPES = ['https://www.googleapis.com/auth/spreadsheets.readonly']
    
    sheet = _create_gsheet_object(path_to_credentials, scopes=SCOPES)
    
    name_range = sheet_name + '!' + sheet_range if sheet_range else sheet_name
    
    result = sheet.values().get(spreadsheetId=spreadsheet_id, range=name_range).execute()
    
    df = pd.DataFrame(result['values'])
    
    if first_row_is_header:
    
        df.columns = df.loc[0]
        
        df = df.drop(0)

    return df