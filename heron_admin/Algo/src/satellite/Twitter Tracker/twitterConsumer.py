from requests_oauthlib import OAuth1Session
import os
import json
from dotenv import load_dotenv

import tweepy

load_dotenv()

# Import environment variables
consumer_key = os.environ.get("TWITTER_CONSUMER_KEY")
consumer_secret = os.environ.get("TWITTER_CONSUMER_SECRET")
twitter_secret_token = os.environ.get("TWITTER_SECRET_ACCESS_TOKEN")
twitter_bearer_token = os.environ.get("TWITTER_BEARER_TOKEN")
twitter_access_token = os.environ.get("TWITTER_ACCESS_TOKEN")

# Get Authentication
auth = tweepy.OAuthHandler(consumer_key, consumer_secret)
auth.set_access_token(twitter_access_token, twitter_secret_token)
api = tweepy.API(auth, wait_on_rate_limit=True)
