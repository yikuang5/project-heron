import requests
import re
from bs4 import BeautifulSoup
import pandas as pd

"""

What is this?
=================================

This is Heron's ethanol data module for retrieving weekly ethanol production levels and ending stocks in Continental United States

"""


def ethanol(url):
    """
    Example 
    ----------------------------------------------------------------------------
    ## Weekly Ethanol Production Levels 
    >>> ethanolProduction = ethanol("https://www.eia.gov/dnav/pet/hist/LeafHandler.ashx?n=pet&s=w_epooxe_yop_nus_mbbld&f=w").applymap(lambda x: x.replace(",",""))

    ## Weekly Ethanol Ending Stocks
    >>> ethanolStocks = ethanol("https://www.eia.gov/dnav/pet/hist/LeafHandler.ashx?n=PET&s=W_EPOOXE_SAE_NUS_MBBL&f=W").applymap(lambda x: x.replace(",",""))
    """

    response = requests.get(url).text

    soup = BeautifulSoup(response)

    data = []
    table = soup.find_all("table")
    table_body = soup.find('tbody')

    rows = table_body.find_all('tr')
    for row in rows:
        cols = row.find_all('td')
        cols = [ele.text.strip() for ele in cols]
        data.append([ele for ele in cols if ele])

    columns = ['year-month', 'week1_enddate', 'week1_value', 'week2_enddate', 'week2_value',
               'week3_enddate', 'week3_value', 'week4_enddate', 'week4_value', 'week5_enddate', 'week5_value']
    ethanol = pd.DataFrame(data, columns=columns).dropna(subset=['year-month'])

    ethanol['year'] = ethanol['year-month'].apply(lambda x: x.split("-")[0])
    ethanol['month'] = ethanol['year-month'].apply(lambda x: x.split("-")[1])

    weeks_col = ['week1_enddate', 'week2_enddate',
                 'week3_enddate', 'week4_enddate', 'week5_enddate']

    data = []
    for week in weeks_col:
        ethanol[week] = ethanol[week].apply(
            lambda x: "-".join(x.split("/")) if x != None else x)
        ethanol[week] = ethanol['year'] + "-" + ethanol[week]
        ethanolItem = ethanol[[week, week.replace("enddate", "value")]]
        data.append(ethanolItem.rename(
            columns={week.replace("_enddate", "_value"): "value", week: "date"}))

    fuelethanol_production = pd.concat(data).dropna().sort_values("date")

    return fuelethanol_production
