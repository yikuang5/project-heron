from datetime import datetime
import talib


class Strategy:

    def __init__(self, aggression):
        pass

    def setData(self, df):
        ''' Declare dataset and prep indicators here 

        Input
        -------------
        df: Pandas dataframe containing date, open, high, low, close columns

        Indicators
        -------------
        Exponential Moving Average: EMALong, EMAShort 
        Average Directional Index: ADX
        Relative Strength Index: RSI

        '''

        df.time = df.time.apply(
            lambda x: datetime.strptime(str(x), "%Y-%m-%d %H:%M:%S"))

        # EMA long and short
        emalong = talib.EMA(df.close, 26)
        emashort = talib.EMA(df.close, 7)

        # Momentum Indicator
        adx = talib.ADX(df.high, df.low, df.close)
        rsi = talib.RSI(df.close)

        # Chaikin Volume Oscillator
        chaikin = talib.ADOSC(df.high, df.low, df.close, df.volume)
        # Period = 10, nbdevup & nbdevdn = 2
        upper, middle, lower = talib.BBANDS(df.close)
