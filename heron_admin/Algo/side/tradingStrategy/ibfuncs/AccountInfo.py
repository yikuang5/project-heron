import pandas as pd 
import numpy as np 

from ib_insync import IB, util
from ib_insync.contract import *  

def get_OpenPositions(ibObj):
    
    allDats = []
    
    OpenPositions = ibObj.positions()
    
    for i in range(len(OpenPositions)):
        
        secType = OpenPositions[i].contract.secType
        symbol = OpenPositions[i].contract.symbol
        lastTradeDate = OpenPositions[i].contract.lastTradeDateOrContractMonth
        reformatDate = lastTradeDate[:4] + "-" + lastTradeDate[4:6] + "-" + lastTradeDate[6:]
        
        positions = OpenPositions[i].position
        
        avgCost = OpenPositions[i].avgCost
        
        allDats.append((secType, symbol, reformatDate, positions, avgCost))
        
    return allDats