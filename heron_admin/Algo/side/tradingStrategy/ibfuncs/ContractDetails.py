import pandas as pd 
import numpy as np 

from ib_insync import IB, util
from ib_insync.contract import *  
from ib_insync.order import MarketOrder

def FutureContractDetails(ibObject, symbol: str, exchange: str = "ECBOT") -> "DataFrame":
    
    """ Returns the contract details of futures
    For Futures Cut Off Dates, refer to IBKR here: https://www.interactivebrokers.com/en/index.php?f=1567&p=physical
    
    Connect to IBKR API
    
    Parameters
    ----------    
    symbol    : Ticker Symbol of the future

    Output
    ----------   
    Output : NaN padded 1D array of EMA values
    """
    
    C = Contract(exchange = exchange, secType = "FUT", symbol = symbol)

    Details = ibObject.reqContractDetails(C)
    Details.sort(key=lambda CD: CD.contract.lastTradeDateOrContractMonth)
    Contracts = [(CD.contractMonth, CD.contract.symbol, CD.contract.conId, CD.contract.lastTradeDateOrContractMonth, CD.contract.multiplier, CD.minTick) for CD in Details]

    return pd.DataFrame(Contracts, columns = ['contractMonth', 'Symbol', 'ContractID', 'ContractDate', 'ContractSize', 'MinimumTick'])