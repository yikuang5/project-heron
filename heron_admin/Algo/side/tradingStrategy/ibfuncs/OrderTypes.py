import pandas as pd 
import numpy as np 

from ib_insync import IB, util
from ib_insync.contract import *  
from ib_insync.order import MarketOrder

class CreateOrder:
    
    ## https://algotrading101.com/learn/ib_insync-interactive-brokers-api-guide/ Some ideas are borrowed from this url 
    
    """
    Creates a class for the creation of different order types 
    ____________________________________________________________
    
    For Futures Cut Off Dates, refer to IBKR here: https://www.interactivebrokers.com/en/index.php?f=1567&p=physical
    For Margin Requirements, refer to IBKR here: https://www.interactivebrokers.com/en/index.php?f=26662 
    """
    
    def __init__(self, ibObject):
        
        """
        Connect to the IBKR API 
        """
        
        self.ib = ibObject
    
    def OrderStatus(self, trade):
        
        """ Outputs the current status of the order, whether it is filled or not 

        Output
        ----------
        Output : Order status // 2021-06-30 07:55:12.318779+00:00 - SLD EUR 200.0 @ 1.1902 
        """
        
        self.trade = trade
        
        if self.trade.orderStatus.status == 'Filled':
            fill = self.trade.fills[-1]
            
            pd.DataFrame([[fill.time, fill.execution.side, fill.contract.symbol, fill.execution.shares, fill.execution.avgPrice]], columns = ['time', 'execution', 'symbol', 'shares', 'price']).to_csv("OrderHistories/PaperTradeOrderHistory.csv", index = False, headers = None, mode = "a")
            print(f'{fill.time} - {fill.execution.side} {fill.contract.symbol} {fill.execution.shares} @ {fill.execution.avgPrice}')
    
    def BasicBuySell(self, contract, buySell, shareSize):
        
        """ Create a basic buy and sell order at the current price 
        
        Parameters
        ----------    
        buySell : "BUY" or "SELL" order
        shareSize : The number of shares to execute

        Output
        ----------   
        Output : Order Status // 2021-06-30 07:55:12.318779+00:00 - SLD EUR 200.0 @ 1.1902 
        
        """
            
        order =  MarketOrder(buySell, shareSize)
        
        trade = self.ib.placeOrder(contract, order) ## Place the trade 
        trade.filledEvent+= self.OrderStatus ## Callback Function to show trade status 
    
    def TakeProfitStopLoss(self, contract, buySell, shareSize, limitPrice, takeProfitPrice, stopLossPrice):
        
        """ Create a take profit or stop loss order 
        
        Parameters
        ----------    
        buySell : "BUY" or "SELL", this is the main order type we want to first execute
        shareSize : The number of shares to execute
        limitPrice : The price at which we want to buySell ("BUY" or "SELL")
        takeProfitPrice : The top bracket, the price we want to take profit
        stopLossPrice : The bottom bracket, the price we want to sell

        Output
        ----------   
        Output : Order Status // 2021-06-30 07:55:12.318779+00:00 - SLD EUR 200.0 @ 1.1902
        
        """
        
        bracketOrder = self.ib.bracketOrder(buySell,
                                            shareSize,
                                            limitPrice = limitPrice,
                                            takeProfitPrice = takeProfitPrice,
                                            stopLossPrice = stopLossPrice)
        for subOrder in bracketOrder:
            trade = self.ib.placeOrder(contract, subOrder)
            trade.filledEvent+= self.OrderStatus