import warnings
import pandas as pd
import numpy as np
from tradingStrategy import TechnicalIndicators
from tradingStrategy.ibfuncs import OrderTypes

warnings.filterwarnings("ignore")


class Trade:

    def __init__(self, ibObj, contract, threshold: str, lperiod: int = 26, speriod: int = 7):
        """ Initialise the initial trading storage variables here. 

        Parameters
        ----------------------------
        lperiod    : The long period time window we want to use 
        speriod    : The short period time window we want to use

        """
        # Params
        self.lperiod, self.speriod, self.threshold = lperiod, speriod, threshold

        # Cue Constants
        self.overallPosition, self.profit = 0, 0
        self.longPrice, self.shortPrice = [], []
        self.buyCloseCue, self.sellCloseCue = 0, 0

        self.position = {0: "Neutral", 1: "Long", 2: "Short"}

        # Define the indicators we wish to use
        self.emaObject = TechnicalIndicators.ExponentialMovingAverage()

        # Create buy sell order object
        self.order = OrderTypes.CreateOrder(ibObj)
        self.contract = contract

    def data_Attributes(self, arr):

        self.emaSlow, self.emaFast = self.emaObject.ema(
            arr, window=self.lperiod, beta=2), self.emaObject.ema(arr, window=self.speriod, beta=2)

        return self.emaSlow, self.emaFast

    def MinMaxNormalize(self, arr, window: int):

        return np.append(np.array([np.nan] * window), (arr[window:] - arr[window:].min())/(arr[window:].max() - arr[window:].min())) * 100

    def tradingStrategy(self, arr):
        """ Program our open position rules here 

        Parameters
        ----------------------------
        arr        : 1D numpy array

        Output 
        ----------------------------
        string     : "Buy" | "Sell" | "Hold" for the buy hold or sell signal

        """

        if np.nan in arr:

            # Make sure the latest 100 available data points
            print("Awaiting full data points... ...")

        else:

            self.emaSlow, self.emaFast = self.emaObject.ema(
                arr, window=self.lperiod, beta=2), self.emaObject.ema(arr, window=self.speriod, beta=2)

            self.fall, self.rise = self.emaObject.emaCrossOver(
                self.emaFast, self.emaSlow)
            self.diff, self.signaline, self.distancefromSignal = self.emaObject.MACD(
                self.emaFast, self.emaSlow, window=14, beta=2)

            def emaOpenPositionCondition(arr):
                """
                Parameters
                ----------------------------
                arr        : Takes the difference arrays
                """

                # Diff is Fast - Slow, When Fast > Slow, Uptrend.
                if arr[-1] < 0 and arr[-2] > 0:

                    emaBuy, emaSell = False, True

                elif arr[-1] > 0 and arr[-2] < 0:

                    emaBuy, emaSell = True, False

                else:

                    emaBuy, emaSell = False, False

                return emaBuy, emaSell

            emaBuy, emaSell = emaOpenPositionCondition(self.diff)

            if emaSell:

                return "Sell"

            elif emaBuy:

                return "Buy"

            else:

                return "Hold"

    def OpenCloseStrategy(self, arr):
        """ Implement trailing stop loss strategy, with the cue set by self.threshold, the absolute value 

        Parameters
        ----------------------------
        arr        : 1D numpy array

        """

        strat = self.tradingStrategy(arr)

        if self.overallPosition == 0:  # Open the position

            if strat == "Buy":

                self.overallPosition += 1  # Buy Order

                self.order.BasicBuySell(
                    self.contract, "BUY", 1)  # Open a long order

                self.longPrice.append(arr[-1])  # Append the price for record
                self.buyCloseCue = arr[-1]  # Init the closing cue

                print(f"Long Trade Opened at ${arr[-1]}")

            elif strat == "Sell":

                self.overallPosition += - 1  # Short Order

                self.order.BasicBuySell(
                    self.contract, "SELL", 1)  # Open a short order

                self.shortPrice.append(arr[-1])
                self.sellCloseCue = arr[-1]

                print(f"Short Trade Opened at ${arr[-1]}")

            else:

                pass

        elif self.overallPosition == 1:  # Determine the closing state

            change = arr[-1] - arr[-2]

            if arr[-1] > self.buyCloseCue:

                self.buyCloseCue = arr[-1]

            elif arr[-1] < self.buyCloseCue:

                dipFromLastRise = arr[-1] - self.buyCloseCue

                if dipFromLastRise <= - self.threshold:

                    self.order.BasicBuySell(self.contract, "SELL", 1)

                    self.overallPosition += - 1  # Close the position
                    self.profit += arr[-1] - self.longPrice[-1]

                    print(
                        f"Long Trade Closed at ${arr[-1]}, profit from trade is ${arr[-1] - self.longPrice[-1]}")

                else:

                    pass

        elif self.overallPosition == - 1:  # If long position currently

            change = arr[-1] - arr[-2]

            if arr[-1] < self.sellCloseCue:

                # If the current price is less than the sell close cue, then update this increase in profit
                self.sellCloseCue = arr[-1]

            elif arr[-1] > self.sellCloseCue:

                dipFromLastRise = arr[-1] - self.sellCloseCue  # Get the loss

                if dipFromLastRise >= self.threshold:  # If it rises $2 or more above the worse price

                    # Close the short order, buy back the shares
                    self.order.BasicBuySell(self.contract, "BUY", 1)

                    self.overallPosition += 1
                    self.profit += self.shortPrice[-1] - arr[-1]

                    print(
                        f"Short Trade Closed at ${arr[-1]}, profit from trade is ${self.shortPrice[-1] - arr[-1]}")

                else:

                    pass

        print(
            f"Current 30s closing price is ${arr[-1]}. Total Profits to date ${self.profit}, position is currently {self.position[self.overallPosition]}")
