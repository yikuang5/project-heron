import numpy as np


def RelativeStrengthIndex(arr: np.ndarray, window: int) -> np.ndarray:
    """ Calculate Relative Strength Index using Numpy Only 

    Parameters
    ----------    
    arr       : input 1D array
    window    : int of interval

    Output
    ----------   
    Output : NaN padded 1D array of RSI values

    """

    shiftArr = np.append(np.nan, arr[:len(arr) - 1])
    priceDiff = (arr - shiftArr)[1:]  # Cut out the first element
    upOrDown = priceDiff > 0

    firstEl = priceDiff[:window]

    AU, AD = np.empty(arr.shape[0], np.float64), np.empty(
        arr.shape[0], np.float64)
    AU.fill(np.nan)
    AD.fill(np.nan)

    AUt, ADt = firstEl[firstEl > 0].mean(), abs(firstEl[firstEl < 0].mean())

    AU[window - 1] = AUt
    AD[window - 1] = ADt

    for s in range(window, arr.shape[0]):

        if arr[s] - arr[s - 1] > 0:
            AUt = (AUt * (window - 1) + arr[s] - arr[s - 1]) / window
            ADt = (ADt * (window - 1)) / window

        elif arr[s] - arr[s - 1] < 0:
            ADt = (ADt * (window - 1) + abs(arr[s] - arr[s - 1])) / window
            AUt = (AUt * (window - 1)) / window

        AU[s] = AUt
        AD[s] = ADt

    RS = AU / AD
    RSI = 100 * (RS / (1 + RS))

    return RSI


class ExponentialMovingAverage:

    """
    Nested pure numpy EMA methods, with implementations of: 

    -------------- Pure EMA Methods --------------

    ema                      - Exponential Moving Average with Beta (Weight of current day event) implementation
    searchEMAsequence        - Vector search for patterns within numpy array 
    emaCrossOver             - Return Index values of days with possible rise and fall of prices
    emaGridSearch            - Searches for the optimum interval of ema crossovers based on historical data in array

    ----------- EMA Derivative Methods -----------

    MACD                     - Moving Average Convergence Divergence 
    """

    def ema(self, arr: np.ndarray, window: int, beta: int) -> np.ndarray:
        """ Calculate exponential moving average using NumPy only.

        Parameters
        ----------    
        arr       : input 1D array
        window    : int of interval
        beta      : int of weightage given to current day ema

        Output
        ----------   
        Output : NaN padded 1D array of EMA values

        """

        arrShape = arr.shape[0]
        returnArray = np.empty(arrShape, np.float64)
        returnArray.fill(np.nan)

        returnArray[window - 1] = arr[:window].mean()

        e = arr[window - 1]

        alpha = beta / float(window + 1)

        for s in range(window, arr.shape[0]):
            e = ((arr[s] - e) * alpha) + e
            returnArray[s] = e

        return returnArray

    def searchSequence(self, arr: np.ndarray, seq: np.ndarray):
        """ Find sequence in an array using NumPy only.

        Parameters
        ----------    
        arr    : input 1D array
        seq    : input 1D array

        Output
        ----------    
        Output : 1D Array of indices that corresponds to the index positions of EMA crossovers, rise and fall
        In case of no match, an empty list is returned.

        """

        # Store sizes of input array and sequence
        Na, Nseq = arr.size, seq.size

        # Range of sequence
        r_seq = np.arange(Nseq)

        # Create a 2D array of sliding indices across the entire length of input array.
        # Match up with the input sequence & get the matching starting indices.
        M = (arr[np.arange(Na - Nseq + 1)[:, None] + r_seq] == seq).all(1)

        # Get the range of those indices as final output
        if M.any() > 0:
            return np.where(np.convolve(M, np.ones((Nseq), dtype=int)) > 0)[0][::Nseq]
        else:
            return None

    def emaCrossOver(self, ema_fast: np.ndarray, ema_slow: np.ndarray) -> np.ndarray:
        """ Pure EMA Crossover, available as MACD

        Parameters
        ----------    
        ema_fast    : input 1D array with the shorter period ema
        ema_slow    : input 1D array with the longer period ema

        Output
        ----------    
        Output : 2 1D arrays of indices containing index positions of where to buy/sell

        """

        diff = np.array(ema_fast) - np.array(ema_slow)
        neg_pos = diff > 0

        fall = self.searchSequence(neg_pos, np.array([True, False, False]))
        rise = self.searchSequence(neg_pos, np.array([False, True, True]))

        return fall, rise

    def emaGridSearch(self, arr: np.ndarray):
        """ Grid search to find the best ema double crossover combination. Optimises based on a comparison of expected crossover and actual crossovers based on historical data

        Parameters
        ----------    
        arr       : input 1D array

        Output
        ----------
        Output: Tuple containing a fast-slow pair of EMA intervals 

        """
        return None

    def MACD(self, ema_fast: np.ndarray, ema_slow: np.ndarray, window: int = 7, beta: int = 2) -> np.ndarray:
        """ Moving Average Convergence Divergence

        Parameters
        ----------    
        ema_fast    : input 1D array with the shorter period ema
        ema_slow    : input 1D array with the longer period ema
        window      : int for signal line smoothing, default 7 
        beta        : int of weightage given to signal line ema, default 2

        Output
        ----------    
        Output :    diff               - the difference between the faster term ema and shorter term ema (+ indicates positive momentum)
                    signaline          - the ema smoothed diff, default to a window period of 7
                    distancefromSignal - the distance of diff from the signal line

        """

        diff = np.array(ema_fast) - np.array(ema_slow)
        signaline = self.ema(diff, window, beta)
        distancefromSignal = diff - signaline

        return diff, signaline, distancefromSignal


def ADX(array: np.array, window, beta):
    """ Directional Movement Index (DMI) calculations 
    Based on formulas https://corporatefinanceinstitute.com/resources/knowledge/trading-investing/directional-movement-index-dmi/

    Parameters
    ----------    
    array    : input 1D array 

    Output
    ----------    
    Output :    DIplus           - Directional Index plus
                DIminus          - Directional Index minus
                ADX              - Average Directional Index
    """

    emaObj = ExponentialMovingAverage()

    high = array.High.values
    low = array.Low.values
    close = array.Close.values

    highPrev = np.append(np.nan, high[:len(high) - 1])
    lowPrev = np.append(np.nan, low[:len(low) - 1])

    plusDM = high - highPrev  # Current high minus previous high
    plusDM[plusDM < 0] = 0  # If less than 0 then -
    minusDM = lowPrev - low  # Previous Low minus current low
    minusDM[minusDM < 0] = 0  # If less than 0 then 0

    # If both +DM and -DM > 0 and +DM > -DM, then +DM = CurrHigh - PrevHigh && -DM = 0
    minusDM[(plusDM > 0) & (minusDM > 0) & (plusDM > minusDM)] = 0
    plusDM[(plusDM > 0) & (minusDM > 0) & (
        minusDM > plusDM)] = 0  # Rule is converse

    smoothed_TR = np.append(np.nan, emaObj.ema(
        TR[1:], window=window, beta=beta))
    smoothed_DMminus = np.append(np.nan, emaObj.ema(
        minusDM[1:], window=window, beta=beta))
    smoothed_DMplus = np.append(np.nan, emaObj.ema(
        plusDM[1:], window=window, beta=beta))

    DIplus = (smoothed_DMplus/smoothed_TR) * 100
    DIminus = (smoothed_DMminus/smoothed_TR) * 100

    DX = (abs(DIplus - DIminus)/(abs(DIplus + DIminus))) * 100

    ADX = np.append([np.nan] * window,
                    emaObj.ema(DX[window:], window=window, beta=beta))

    return DIminus, DIplus, ADX
