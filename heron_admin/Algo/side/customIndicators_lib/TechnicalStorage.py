def Modified_Bollinger(period=str(),Ticker=str()):
    
    days = 20 
    EMAdays = 20 # 10 -30 days range 
    weight_day = 3 
    Stdev = 1
    weights = float((weight_day/(days+(weight_day-1))))
    ticker = pd.DataFrame(yf.Ticker(Ticker).history(period=period))

    # Modified Bollinger Bands 

    weighted_stdev_list=[]
    for i in range(0,days-1):
        weighted_stdev_list.append('NaN')

    for a in range(0,len(ticker)-days+1): # +1 to account for the usage of the days.th value
        print(a)
        sample = list(ticker['Close'][a:days+a])
        assert(len(sample)==days)
        Summed_Value = 0 
        for i in range(0,len(sample)-1): # Minus one to leave out the latest value 
            Summed_Value += ( 1 / (days+(weight_day-1)) ) * ( (sample[i] - np.average(sample))**2 ) # Get the first 4 earlier values
        Summed_Value += ( weights * (sample[len(sample)-1] - np.average(sample)) **2 ) # Numerator of weighted standard deviation
        weighted_stdev = np.sqrt( Summed_Value / ( (days - 1)/days ) )
        weighted_stdev_list.append(weighted_stdev)
    assert(len(weighted_stdev_list)==len(ticker))
    
    TopBollinger,BottomBollinger = [],[]

    CenterLine = twisted.EMA(ticker,no_of_days = EMAdays)
    for i in range(len(weighted_stdev_list)):
        if weighted_stdev_list[i]=='NaN' :
            TopBollinger.append('NaN')
            BottomBollinger.append('NaN') 
        else:
            TopBollinger.append( float(CenterLine[i]) + ( Stdev * weighted_stdev_list[i])) # 1 Standard Deviation away, toggle this
            BottomBollinger.append( float(CenterLine[i]) - ( 1 * weighted_stdev_list[i]))
    assert(len(TopBollinger)==len(BottomBollinger)==len(weighted_stdev_list)==len(ticker))

    return TopBollinger,BottomBollinger,CenterLine


def Stochastic(ticker=str(),period=str(),days=int()):
    raw_stochastic= []
    ticker = pd.DataFrame(yf.Ticker(ticker).history(period=period))

    maximum_list,minimum_list = [],[]
    for i in range(0,days):
        raw_stochastic.append('NaN') # Calculates the % K 
        minimum_list.append(ticker['Low'][i])
        maximum_list.append(ticker['High'][i])

    raw_stochastic.remove('NaN') # Check this to see if length tallies 

    max_val,min_val = max(maximum_list),min(minimum_list) # Find the first maximum day
    first_close = ticker['Close'][days-1]
    first_stochastic = (( first_close - min_val )/( max_val - min_val )) * 100 # Calculates the first Stochastic Value 
    raw_stochastic.append(first_stochastic)
    
    for i in range(days,len(ticker)):
        min_list,max_list=[],[]
        for a in range(i-days,i+1): # Check the minimum and maximum values for the past n days
            min_list.append(ticker['Low'][a])
            max_list.append(ticker['High'][a])
        min_val = min(min_list)
        max_val = max(max_list)
        raw_stochastic_value = (( ticker['Close'][i] - min_val )/( max_val - min_val )) * 100
        raw_stochastic.append(raw_stochastic_value)

    if len(raw_stochastic) != len(ticker): # Test Check 
        raise ValueError("Check length of stochastic list and length of ticker list, length of raw stochastic: "+len(raw_stochastic)+" length of ticker dataframe: "+len(ticker))
    
    slow_stochastic,slow_slow_stochastic = [],[]
    for i in range(0,days + 2): 
        slow_stochastic.append('NaN') # Calculates the % D = % K slow
    
    for i in range(days + 2,len(raw_stochastic)): 
        sum = raw_stochastic[i] + raw_stochastic[i-1] + raw_stochastic[i-2]
        slow_stochastic_value = sum / 3
        slow_stochastic.append(slow_stochastic_value)

    for i in range(0,days + 4):
        slow_slow_stochastic.append('NaN') # Calculates the % D-Slow
    
    for i in range(days + 4,len(slow_stochastic)):
        sum = slow_stochastic[i] + slow_stochastic[i-1] + slow_stochastic[i-2]
        slow_slow_stochastic_value = sum / 3
        slow_slow_stochastic.append(slow_slow_stochastic_value)
    
    if len(raw_stochastic) != len(slow_stochastic) or len(slow_stochastic) != len(slow_slow_stochastic): 
        raise ValueError("Length of stochastic lists do not tally, check the 3 lists...")
    
    return raw_stochastic, slow_stochastic, slow_slow_stochastic