from datetime import datetime as dt
from datetime import timedelta
from datetime import date
import time

import pandas as pd

from findiff import FinDiff
from decimal import *
import numpy as np

def FindInflexionPoints(arr, accuracy: int) -> np.array:
    
    """ Finding Peak and Trough Points 
    
    Parameters
    ----------    
    arr         : 1D numpy array
    accuracy    : the dy/dx accuracy

    Output
    ----------   
    Output : np array containing the index values of both the Peak and Trough values
    
    """
    
    dx = 1 # 1 day interval
    d_dx = FinDiff(0, dx, 1, acc = accuracy) # Optimal Accuracy Order is between 4 & 5, with higher orders incurring higher mistakes - 2 is by default the Acc Order
    d2_dx2 = FinDiff(0, dx, 2, acc = accuracy)
    
    FirstDeriv = d_dx(arr)
    SecondDeriv = d2_dx2(arr)
    
    Peakvalues, Troughvalues = [], []
    
    for x in range(len(FirstDeriv)):
        
        if SecondDeriv[x] > 0 and (FirstDeriv[x] == 0 or 
            (x != len(FirstDeriv) - 1 and 
            (FirstDeriv[x] > 0 and FirstDeriv[x + 1] < 0 and arr[x] >= arr[x +1 ] or
            FirstDeriv[x] < 0 and FirstDeriv[x + 1] > 0 and arr[x] <= arr[x + 1]) or
            x != 0 and 
            (FirstDeriv[x - 1] > 0 and FirstDeriv[x] < 0 and arr[x - 1] < arr[x] or
            FirstDeriv[x - 1] < 0 and FirstDeriv[x] > 0 and arr[x - 1] > arr[x]))):
            
            Troughvalues.append(x)
            
        elif SecondDeriv[x] < 0 and (FirstDeriv[x] == 0 or 
            (x != len(FirstDeriv) - 1 and 
            (FirstDeriv[x] > 0 and FirstDeriv[x + 1] < 0 and arr[x] >= arr[x + 1] or
            FirstDeriv[x] < 0 and FirstDeriv[x + 1] > 0 and arr[x] <= arr[x + 1]) or
            x != 0 and 
            (FirstDeriv[x - 1] > 0 and FirstDeriv[x] < 0 and arr[x - 1] < arr[x] or
            FirstDeriv[x - 1] < 0 and FirstDeriv[x] > 0 and arr[x - 1] > arr[x]))):
            
            Peakvalues.append(x)

    return np.array(Peakvalues), np.array(Troughvalues)

def HoughLineTransform(Xarr: np.array, Yarr: np.array, bounds: int = 5):
    
    """ Hough Line Transform specialised for Time Series
        
    Parameters
    ----------  
    Xarr      : input 1D array containng X positions denoted integers
    Yarr      : input 1D array containing Y positions usually the Closing values of found inflexion points; see function FindInflexionPoints
    bounds.   : the number of largest elements we want to get from the transform, might include overlapping elements. A larger bound will result in more hough lines

    Output
    ----------   
    Output : sinValues, cosValues, rhoValues, all numpy arrays containing the relative values for the lines. Where y = (rho - x * cos(theta)) / sin(theta)
    
    """
    
    def getnLargest(arr: np.array, nlargest: int) -> np.array:

        """ Get the nlargest elements in an numpy array

        Parameters
        ----------    
        arr      : input 1D array
        nlargest : the number of elements to return 

        Output
        ----------   
        Output : numpy array of sinvalues, cosvalues and rhovalues

        """

        sortedArr = arr.flatten()
        sortedArr.sort()

        return sortedArr[-nlargest:]
    
    # Get boundaries for Rho values
    width, height = Xarr.max(), int(Yarr.max())
    diagonal_length = int(np.ceil(np.sqrt(width ** 2 + height ** 2)))

    # Get Range of Thetas and Rho
    thetas = np.deg2rad(np.arange(-90, 90))
    rhos = np.linspace(-diagonal_length, diagonal_length, diagonal_length * 2)

    cos_theta, sin_theta = np.cos(thetas), np.sin(thetas)

    costheta_x = cos_theta.reshape((cos_theta.shape[0], 1)) * Xarr
    sintheta_y = sin_theta.reshape((sin_theta.shape[0], 1)) * Yarr

    rho_pdt = (costheta_x + sintheta_y + diagonal_length).astype(int)

    accumulator = []
    
    for z in range(len(rho_pdt)):
        
        rhoPdt = np.bincount(rho_pdt[z]) # https://numpy.org/doc/stable/reference/generated/numpy.bincount.html : Returns the index voted in a single 1D accumulator 
        accumulator.append(np.pad(rhoPdt, (0, 2 * diagonal_length - len(rhoPdt)), 'constant'))

    accumulator = np.array(accumulator)  # 2D numpy array of accumular values (theta_index_value, rho_value)
    
    ### Accumulator result processing 
    largestNvalues = set(getnLargest(accumulator, bounds)) # Get the element values of n largest in accumulator 
    print(f"Most intersections within these {bounds} are: {largestNvalues}")
    indexPositions = np.array([np.where(accumulator == largeElement) for largeElement in largestNvalues], dtype = list) # Get the index position of these n largest element values in accumulator 
    
    thetaValues, rhoValues = [], []
    ## Unpack the values to theta and rho values of max intersection points
    for index in indexPositions: 
        for a, b in zip(index[0], index[1]):
            thetaValues.append(a)
            rhoValues.append(b)

    sinValues, cosValues, rhoValues = [sin_theta[i] for i in thetaValues], [cos_theta[i] for i in thetaValues], [rhos[i] for i in rhoValues]
    
    return np.array(sinValues), np.array(cosValues), np.array(rhoValues)

def slidingWindow(arr, window_size):
    
    """ Sliding Window on a given numpy array
        
    Parameters
    ----------    
    arr             : input 1D array
    window_size     : window size 

    Output
    ----------   
    Output : 2 Dimensional numpy array containing the values 
    
    """
    
    start = 0 
    max_time = len(arr) - window_size 
    
    sub_windows = (
        start +
        # expand_dims are used to convert a 1D array to 2D array.
        np.expand_dims(np.arange(window_size), 0) +
        np.expand_dims(np.arange(max_time + 1), 0).T
    )
    
    return array[sub_windows]