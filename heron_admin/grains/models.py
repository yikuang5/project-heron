from django.db import models

# Create your models here.


class NASSQuickStats(models.Model):
    STATE_FIPS_CODE = models.CharField(max_length=150)
    STATE_NAME = models.CharField(max_length=150)
    FREQ_DESC = models.CharField(max_length=150)
    LOAD_DESC = models.DateTimeField(default=None, null=True)
    YEAR = models.IntegerField(default=None, null=True)
    WEEK = models.IntegerField(default=None, null=True)
    WEEK_ENDING = models.DateTimeField(default=None, null=True)
    STAT_DESC = models.CharField(max_length=150)
    COMMODITY_DESC = models.CharField(max_length=150)
    UNIT_DESC = models.CharField(max_length=150)
    VALUE = models.FloatField(null=True)


class ESRUSWeeklyExports(models.Model):
    DATEFIELD = models.DateField()
    COMMODITY = models.CharField(max_length=150)
    COUNTRY = models.CharField(max_length=150)
    WEEKLYEXPORTS = models.IntegerField(default=None, null=True)
    ACCUMULATEDEXPORTS = models.IntegerField(default=None, null=True)
    OUTSTANDINGSALES = models.IntegerField(default=None, null=True)
    GROSSNEWSALES = models.IntegerField(default=None, null=True)
    CURRENTMYNETSALES = models.IntegerField(default=None, null=True)
    CURRENTMYTOTALCOMMITMENT = models.IntegerField(default=None, null=True)
    NEXTMYTOTALOUSTANDINGSALES = models.IntegerField(default=None, null=True)
    NEXTMYNETSALES = models.IntegerField(default=None, null=True)


class NewsData(models.Model):
    SGT_DATEFIELD = models.DateTimeField(default=None, null=True)
    SOURCE = models.CharField(
        max_length=150, default=None, null=True, blank=True)
    AUTHOR = models.CharField(
        max_length=150, default=None, null=True, blank=True)
    TITLE = models.CharField(
        max_length=150, default=None, null=True, blank=True)
    DESCRIPTION = models.CharField(
        max_length=150, default=None, null=True, blank=True)
    URL = models.CharField(max_length=150, default=None, null=True, blank=True)
    CONTENT = models.CharField(
        max_length=150, default=None, null=True, blank=True)
