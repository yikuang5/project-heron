from __future__ import absolute_import, unicode_literals

from .apis.news import globalNews, agricensus
from .models import NewsData as NDObject

from datetime import datetime, timedelta
import pandas as pd
import re

from celery import shared_task


@shared_task
def UpdateGlobalNews():
    """ 
    updates the news database
    """
    t = globalNews.NewsData()  # Time Zone compatabilities
    q = globalNews.NewsData(datetime.strftime(
        datetime.now() + timedelta(1), "%Y-%m-%d"))
    a = agricensus.AgricensusHeadlines().rename(columns={"time_before": "SGT"}).fillna(
        "")  # Gets the first page of agricensus headlines
    t = pd.concat([a, q, t]).drop_duplicates(subset=['title'])

    latestNews = globalNews.NewsData()
    existingTitles = pd.DataFrame(
        list(NDObject.objects.all().values())).TITLE.values
    newT = t[~t.title.isin(existingTitles)]

    if len(newT) > 0:
        model_instances = [NDObject(SGT_DATEFIELD=item.SGT, SOURCE=item.source, AUTHOR=item.author,
                                    TITLE=item.title, DESCRIPTION=item.description, URL=item.url, CONTENT=item.content) for item in newT.itertuples()]
        NDObject.objects.bulk_create(model_instances)
        print("News model updated")
    else:
        print("Most recent news data established.")
