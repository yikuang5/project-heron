from django.shortcuts import render
from django.core import serializers
from django.http import HttpResponse, JsonResponse
from django.core import serializers


from .models import NASSQuickStats, ESRUSWeeklyExports, NewsData

from .apis import AgencyReports

from .apis import ProgressCondition

import re
import time
import json
import pandas as pd
from datetime import datetime, timedelta

# Create your views here.


def GrainsAnalysis(request, *args, **kwargs):
    return render(request, 'grains.html')


def agencyReports(request, *args, **kwargs):
    agencyReport = AgencyReports.AgencyReports()  # Event Calendar
    return JsonResponse(agencyReport.T.to_json(), safe=False)


def GlobalNews(request):  # This gets the latest data available in the database
    # Garbage duplicate data fields, which should not occur!
    for title in NewsData.objects.values_list('TITLE', flat=True).distinct():
        NewsData.objects.filter(pk__in=NewsData.objects.filter(
            TITLE=title).values_list('id', flat=True)[1:]).delete()
    p1d_Data = serializers.serialize('json', NewsData.objects.filter(
        SGT_DATEFIELD__gte=datetime.now() - timedelta(1)).order_by('SGT_DATEFIELD').distinct())
    return JsonResponse(p1d_Data, safe=False)


def NASSQuickStats_Query(request):

    if request.method == 'GET':

        e = time.time()
        queryset = NASSQuickStats.objects.all()
        print(f"NASS Query completed in {time.time() - e}s")
        df = pd.DataFrame(list(queryset.values()))

        condition_descs, progress_descs = ProgressCondition.returnConditionProgress(
            df)  # Format the Condition and Progress

        condition_descs = condition_descs.drop_duplicates()
        condition_descs.reset_index(drop=True, inplace=True)
        CONDITION = condition_descs.T.to_json()

        progress_descs = progress_descs.drop_duplicates()
        progress_descs.reset_index(drop=True, inplace=True)
        PROGRESS = progress_descs.T.to_json()

        data = json.dumps({"CONDITION": CONDITION, "PROGRESS": PROGRESS})

        return JsonResponse(data, safe=False)


def USweeklyexports(request, *args, **kwargs):
    f = time.time()
    agencyReport = ESRUSWeeklyExports.objects.all()
    df = pd.DataFrame(list(agencyReport.values()))
    df['YEAR'] = df.DATEFIELD.apply(
        lambda x: re.findall('(.*?)-..-..', str(x))[0])
    df['WEEK'] = df.DATEFIELD.apply(lambda x: x.isocalendar()[1])
    print(f"ESR Query completed in {time.time() - f}s")

    splitbyyear = [dataframe[1] for dataframe in df.groupby("YEAR")]
    new_refreshed = []
    for item in splitbyyear:
        item.reset_index(drop=True, inplace=True)
        new_refreshed.append({item.YEAR[0]: item.T.to_json()})

    return JsonResponse(new_refreshed, safe=False)
