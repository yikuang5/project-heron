from django.contrib import admin
from .models import NASSQuickStats, ESRUSWeeklyExports, NewsData

# Register your models here.
admin.site.register(NASSQuickStats)
admin.site.register(ESRUSWeeklyExports)
admin.site.register(NewsData)
