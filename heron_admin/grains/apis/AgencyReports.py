import re 
import requests 
import pandas as pd
from bs4 import BeautifulSoup 
from datetime import datetime

def AgencyReports():
    
    """ USDA Scheduled Release Dates for Agency Reports and Summaries
    Eastern Time (ET) 

    Outputs
    ---------------------
    AgencyCalendars      : Pandas dataframe containing various factors such as release date, report name and the links to those reports when they do arrive 
    
    """
    
    soup = BeautifulSoup(requests.get("https://www.usda.gov/media/agency-reports").text)

    tag_dates = soup.find_all(lambda tag: tag.name == 'h3' and str(datetime.now().year) in tag.get_text())
    tag_dates = [re.findall(">(.*?)<", str(tag_date))[0] for tag_date in tag_dates]

    agencyReports = soup.find_all("ul", {"class":"agency-reports"})

    assert(len(agencyReports) == len(tag_dates))

    AgencyCalendars = []

    for tag_date, agencyReport in zip(tag_dates, agencyReports):
        for li in agencyReport.find_all("li"):
            ReportName = re.findall('>(.*?)<', str(li.find_all("a")))[0]
            linkToReport = re.findall('href="(.*?)"',str(li.find_all("a", href = True)[0]))[0]
            agencyReportDate = re.findall('>(.*?)<', str(li.find_all("span",{"class":"agency-report-date"})))[0]
            agency = re.findall('>(.*?)<', str(li.find_all("span",{"class":"agency-report-agency"})))[0]

            AgencyCalendars.append((tag_date, ReportName, linkToReport, agencyReportDate, agency))

    AgencyCalendars = pd.DataFrame(AgencyCalendars, columns = ['release_date','report','link','time','agency']).applymap(lambda x: x.replace("\t","").replace("\n","").strip(" "))
    AgencyCalendars.release_date = AgencyCalendars.release_date.apply(lambda x: datetime.strftime(datetime.strptime(x, "%a, %m/%d/%Y"), "%Y-%m-%d") + " Eastern Time (ET)")
    
    return AgencyCalendars

def CropProductionNASS():
    
    """ This monthly report contains crop production data for the U.S., including acreage, area harvested, and yield. 
    Wheat, fruits, nuts, and hops are the specific crops included in the report, but data on planted and harvested crop area are also included. 
    There is also a strong focus on maple syrup, as the report details production, value, season dates, and percent of sales by type. 
    The report also contains a monthly weather summary, a monthly agricultural summary, and an analysis of precipitation and the degree of departure from the normal precipitation map for the month.
    ------------------------------------------------
    FREQUENCY : MONTHLY 12:00 PM Eastern Time (ET)
    ------------------------------------------------
    
    Outputs
    ---------------------
    attr_freq            : str ; The frequency release type, i.e. Monthly, Weekly, Annual (In this case, Monthly)
    attr_upcomingRelease : list ; The Upcoming release dates
    hrefs                : pd.DataFrame ; Pandas Dataframe containing PDF, TXT, ZIP report urls
    
    """
    soup = BeautifulSoup(requests.get("https://usda.library.cornell.edu/concern/publications/tm70mv177?locale=en").text)
    
    attr_upcomingRelease = soup.find_all("span", {"class":"attribute upcoming_releases"})
    attr_upcomingRelease = [re.findall(">(.*?)<", str(attr_ur))[0] for attr_ur in attr_upcomingRelease]

    attr_freq = soup.find_all("span", {"class":"attribute frequency"})[0]
    attr_freq = re.findall(">(.*?)<", str(attr_freq))[0]
    
    table = soup.find_all("table")[0]
    df = pd.read_html(str(table))[0]
    hrefs = np.array([re.findall('href="(.*?)"', str(a))[0] for a in table.find_all('a', href=True)])
    hrefs = pd.DataFrame(np.array_split(hrefs, int(len(hrefs)/4)), columns = ['pdf','txt','zip','drop']).drop(columns = ['drop'])
    
    return attr_freq, attr_upcomingRelease, hrefs