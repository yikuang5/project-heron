import re
import json
import requests
import pandas as pd
import dateutil.parser
from datetime import datetime, timedelta
import warnings
warnings.filterwarnings("ignore")

### Allows 500 requests per key 
with open("./main/apis/news/Credentials.json") as f:
    APIKEYS = json.loads(f.read())
    
global keyState
keyState = 0

global APIKEY
APIKEY = APIKEYS["APIKEY_" + str(keyState)]

def NewsData(date: str = "", 
            query: str = r'''
    ("corn" OR "maize" OR "barley" OR "wheat" OR "sorghum" OR crop OR harvest OR "rice" OR "ethanol" OR "rye" OR "oats" OR "oat" OR "cotton" OR "ground nut" OR "canola" OR sunflower 
    OR "grain" OR "grains" OR "cereal" OR feed OR livestock OR swine OR poultry OR futures OR commodity OR agriculture OR agricultural OR mining OR treasuries OR federal OR 
    oil OR diesel) AND

    ("crop conditions" OR market OR bushels OR contract OR report  OR market
    OR strateg OR industrial OR industry OR futures OR session OR shipment OR "mt"
    OR import OR export OR demand OR supply OR analys OR price OR volatil OR crisis OR output OR stock OR tariff OR policy 
    OR optimist 
    OR drought OR weather OR frost OR heat OR cold OR rain OR flood OR moist OR rust OR disaster OR pest OR infest OR rainfall OR inventories OR inventory OR dry OR wet
    OR rise OR grow OR acre OR farm OR plant OR area OR dump OR burn OR produce OR producing OR produces OR soil OR pressure
    OR low OR high OR loss OR sell OR buy OR ease OR lose OR gain OR drop OR dip OR availabl OR long OR short OR profit OR margin OR steady OR bull OR bear OR yield OR rate)

    NOT (grilled OR menu OR cook OR adorable OR recipe OR bread OR bake OR microwaveable OR shirt OR aromatherapy 
    OR dumpling OR breakfast OR ingredient OR sauce OR flavor OR dessert OR towel OR "wholesale price")
    '''):
    
    """ https://newsapi.org/docs/endpoints/everything
    
    Surround phrases with quotes (") for exact match.
    Prepend words or phrases that must appear with a + symbol. Eg: +bitcoin
    Prepend words that must not appear with a - symbol. Eg: -bitcoin
    Alternatively you can use the AND / OR / NOT keywords, and optionally group these with parenthesis. Eg: crypto AND (ethereum OR litecoin) NOT bitcoin.
    
    """
    
    ### Makes sure that its converted to Eastern Time (ET)
    if date == "":
        date = datetime.strftime(datetime.now() - timedelta(hours = 8), "%Y-%m-%d")
    else:
        date = datetime.strftime(datetime.strptime(date, "%Y-%m-%d") - timedelta(hours = 8), "%Y-%m-%d")
    
    global APIKEY
    global keyState

    country = "us"
    pageSize = 100
    START = END = date
    SORT = "publishedAt"
    language = "en"

    ### Commodity Query
        
    response = requests.get(f"https://newsapi.org/v2/everything?qInTitle={query}&language={language}&apiKey={APIKEY}&from={START}&to={END}&sortBy={SORT}&pageSize={pageSize}").text

    responseEval = eval(response.replace("null","''"))
    
    if responseEval['status'] == "ok": 
        # Articles are sorted by the earliest date published first.
        
        q = pd.DataFrame(responseEval['articles'])
        q["UTC"] = q.publishedAt.apply(lambda x: datetime.strftime(dateutil.parser.parse(x), "%Y-%m-%d %H:%M:%S"))
        q["SGT"] = q.publishedAt.apply(lambda x: datetime.strftime(dateutil.parser.parse(x) + timedelta(hours = 8), "%Y-%m-%d %H:%M:%S"))

        ## Text Processing 
        q.source = q.source.apply(lambda x: x['name'])
        q.title = q.title.apply(lambda x: re.sub('<.*>','', x).replace(u'\xa0', u' ').replace("\n"," .").replace("\r","").replace("\t"," "))
        q.description = q.description.apply(lambda x: re.sub('<.*>','', x).replace(u'\xa0', u' ').replace("\n"," .").replace("\r","").replace("\t"," "))
        q.content = q.content.apply(lambda x: re.sub('<.*>','', x).replace(u'\xa0', u' ').replace("\n"," .").replace("\r","").replace("\t"," "))

        return q.drop(columns = ["publishedAt", "urlToImage"])
    
    elif responseEval['status'] == "error": 
        
        keyState += 1
        
        APIKEY = APIKEYS["APIKEY_" + str(keyState)]
        
        if keyState < len(APIKEYS):
            
            return NewsData()
            
        else:
            
            raise ValueError("Exhausted all API Keys")