import re
import requests 
from bs4 import BeautifulSoup 
import pandas as pd 
from datetime import datetime, timedelta
import warnings
warnings.filterwarnings("ignore")

def preprocessTime(x):

    timenow = datetime.now()

    if "hour" in x:

        t = x.replace("hours ago", "").replace("hour ago","").strip(" ")

        t = timenow - timedelta(hours = int(t)) + timedelta(hours = 8)

        return datetime.strftime(t, "%Y-%m-%d %H:%M:%S")

    elif "minute" in x: 

        t = x.replace("minute ago", "").replace("minutes ago", "").strip(" ")

        t = timenow - timedelta(minutes = int(t)) + timedelta(hours = 8)

        return datetime.strftime(t, "%Y-%m-%d %H:%M:%S")

    else:

        t = datetime.strptime(x, "%d %b %Y") + timedelta(hours = 8)

        return datetime.strftime(t, "%Y-%m-%d %H:%M:%S")


def AgricensusHeadlines():

    agricensus_headlines = BeautifulSoup(requests.get("https://www.agricensus.com/news/").text, features="html.parser").find_all("div", {"class": "news-list-block shadow-box"})

    agricensus_page1_data = []

    for headline in agricensus_headlines:
        
        title = headline.find_all("div", {"class": "news-list-title"})
        auth = headline.find_all("div", {"class": "news-list-date-author"})
        desc = headline.find_all("div", {"class": "news-list-description"})
        
        url_items = headline.select("a[href*=Article]")
        url = [re.findall("href=\"(.*?)\"", str(url_item))[0] for url_item in url_items][0]

        title_clean = [i for i in re.findall(">(.*?)<", str(title[0])) if i != ""][0]
        auth_clean = [i for i in re.findall(">(.*?)<", str(auth[0])) if i != ""][0].replace("|", "").strip(" ")
        desc_clean = [i for i in re.findall(">(.*?)<", str(desc[0])) if i != ""][0]
        url_clean = "https://www.agricensus.com/" + url

        agricensus_page1_data.append((title_clean, url, auth_clean, desc_clean))

    a = pd.DataFrame(agricensus_page1_data, columns = ['title', 'url', 'time_before', 'description'])

    a.time_before = a.time_before.apply(lambda x: preprocessTime(x)) ## Process time strings
    
    a['source'] = "Agricensus"
    a['author'] = "Agricensus"
    a = a.fillna("")

    return a