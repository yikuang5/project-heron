from django.conf.urls import url
from channels.routing import ProtocolTypeRouter, URLRouter
from channels.security.websocket import AllowedHostsOriginValidator, OriginValidator
from channels.auth import AuthMiddlewareStack

from main.consumers import IbinsyncConsumer

application = ProtocolTypeRouter({
    "websocket": AllowedHostsOriginValidator(
        AuthMiddlewareStack(
            URLRouter(
                [
                    url(r"", IbinsyncConsumer()) ## ws://ourdomain/<username> if use ws/(?P<username>[^/]+)/$
                ]
            )
        )
    ),
})