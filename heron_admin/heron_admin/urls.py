"""heron_admin URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/3.2/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path, include
from django.conf.urls import url
from main import views as main_views
from grains import views as grain_views
from rest_framework_simplejwt.views import (TokenObtainPairView, TokenRefreshView,TokenVerifyView)

urlpatterns = [
    path('admin/', admin.site.urls, name = "admin"),
    url(r'^api/globalNews', grain_views.GlobalNews, name = 'GlobalNews'), ## For the global news table on the left in Portfolio Home
    url(r'^api/nassquickstats', grain_views.NASSQuickStats_Query, name = 'nassQuickStats'),
    url(r'^api/agencyreports', grain_views.agencyReports, name = 'agencyReports'),
    url(r'^api/ESRUSWeeklyExports', grain_views.USweeklyexports, name = 'USweeklyexports'),
    url(r'^api/portfolio$', main_views.portfolio_list),
    url(r'^api/portfolio/(?P<orderid>.*)', main_views.portfolio_detail),
    url(r'^api/portfolioposition', main_views.portfolioQuery),
    url(r'^api/tickerlist', main_views.ticker_list),
    path('api/user/', include('users.urls', namespace = 'users')),
    path('api/token/', TokenObtainPairView.as_view(), name='token_obtain_pair'),
    path('api/token/refresh/', TokenRefreshView.as_view(), name='token_refresh'),
    path('api/token/verify/', TokenVerifyView.as_view(), name='token_refresh'),
]