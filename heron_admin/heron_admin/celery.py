from __future__ import absolute_import, unicode_literals

import os
import re
import json
import redis
from ib_insync import *
from dotenv import load_dotenv
from celery import Celery
from django.conf import settings
from asgiref.sync import async_to_sync

# Set the default Django settings module for the 'celery' program.
os.environ.setdefault('DJANGO_SETTINGS_MODULE', 'heron_admin.settings')
app = Celery('heron_admin')
app.config_from_object('django.conf:settings', namespace='CELERY')
# Load task modules from all registered Django apps.
app.autodiscover_tasks(lambda: settings.INSTALLED_APPS)

## celery -A heron_admin worker -l info --pool=solo have to run first !!!!

load_dotenv()

r = redis.Redis(db = 0)

OANDA_ACCNO = os.environ["ACCOUNT_NUMBER"]
OANDA_APIKEY = os.environ["API_KEY"]

@app.task(bind=True)
def debug_task(self):
    print(f'Request: {self.request!r}')