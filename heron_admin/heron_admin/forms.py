from django.forms import ModelForm
from .models import PortfolioPositions

class PortfolioForm(ModelForm):
    class Meta: 
        model = PortfolioPositions
        fields = '__all__'
        

