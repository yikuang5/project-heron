from django.shortcuts import render
from django.core import serializers
from django.http import HttpResponse, JsonResponse
from django.views.decorators.csrf import csrf_exempt

from rest_framework import status
from rest_framework.decorators import api_view, permission_classes
from rest_framework.response import Response
from rest_framework.permissions import IsAuthenticated
from rest_framework.parsers import JSONParser

import ast
import json
import redis
import pandas as pd

from .models import PortfolioPositions, AvailableTickers
from .serializers import PortfolioPositionsSerializer, PortfolioOvPosSerializer, AvailableTickerSerializer

from django.db.models import Case, Value, When, F, Sum

r = redis.Redis()
# https://www.bezkoder.com/django-rest-api/ CRUD


@api_view(['GET', 'PUT', 'DELETE'])
@permission_classes([IsAuthenticated])
def portfolio_detail(request, orderid):
    """
    GET/PUT/DELETE based on OrderId
    """
    print(request.user.is_authenticated)
    try:
        portfoliopos = PortfolioPositions.objects.get(OrderId=orderid)
    except PortfolioPositions.DoesNotExist:
        return JsonResponse({'message': 'The OrderId does not exist'}, status=status.HTTP_404_NOT_FOUND)

    if request.method == 'GET':
        portfoliopos_serializer = PortfolioPositionsSerializer(portfoliopos)
        return JsonResponse(portfoliopos_serializer.data)

    elif request.method == 'PUT':
        portfoliopos_data = JSONParser().parse(request)
        portfoliopos_serializer = PortfolioPositionsSerializer(
            portfoliopos, data=portfoliopos_data)
        if portfoliopos_serializer.is_valid():
            portfoliopos_serializer.save()
            return JsonResponse(portfoliopos_serializer.data)
        return JsonResponse(portfoliopos_serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    elif request.method == 'DELETE':
        portfoliopos.delete()
        return JsonResponse({'message': 'Line was deleted successfully!'}, status=status.HTTP_204_NO_CONTENT)


@api_view(['GET', 'POST', 'DELETE'])
@permission_classes([IsAuthenticated])
def portfolio_list(request):
    """
    GET list of portfolio tickers
    POST a new portfolio entry
    DELETE all portfolio entries
    """
    print(request.user.is_authenticated)
    if request.method == 'GET':
        portfoliopos = PortfolioPositions.objects.all()
        OrderId = request.query_params.get('OrderId', None)
        if OrderId is not None:
            portfoliopos = portfoliopos.filter(OrderId__icontains=OrderId)

        portfoliopos_serializer = PortfolioPositionsSerializer(
            portfoliopos, many=True)
        return JsonResponse(portfoliopos_serializer.data, safe=False)

    elif request.method == 'POST':
        portfolio_data = JSONParser().parse(request)
        portfoliopos_serializer = PortfolioPositionsSerializer(
            data=portfolio_data)
        if portfoliopos_serializer.is_valid():
            portfoliopos_serializer.save()
            return JsonResponse(portfoliopos_serializer.data, status=status.HTTP_201_CREATED)
        else:
            print(portfoliopos_serializer.errors)
            return JsonResponse(portfoliopos_serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    elif request.method == 'DELETE':
        count = PortfolioPositions.objects.all().delete()
        return JsonResponse({'message': '{} Entries were deleted successfully!'.format(count[0])}, status=status.HTTP_204_NO_CONTENT)


@api_view(['GET'])
@permission_classes([IsAuthenticated])
def portfolioQuery(request):
    print(request.user.is_authenticated)
    queryset = PortfolioPositions.objects.annotate(
        PositionalInteger=Case(
            When(Position="Long", then=Value(1)),
            When(Position="Short", then=Value(-1)),
            default=Value(0),),
        PositionalShares=F("PositionalInteger") * F("Shares")
    ).values_list('Symbol', 'PositionalShares', 'FinancialInstrument')

    groupbyquery = queryset.values("Symbol", "FinancialInstrument").order_by(
        "Symbol", "FinancialInstrument").annotate(OverallPosition=Sum("PositionalShares"))
    query_serialized = PortfolioOvPosSerializer(groupbyquery.values(
        "Symbol", "FinancialInstrument", 'OverallPosition'), many=True)
    return JsonResponse(query_serialized.data, safe=False)


@api_view(['GET'])
@permission_classes([IsAuthenticated])
def ticker_list(request):
    """
    GET the available tickers provided by FinnHub
    """
    print(request.user.is_authenticated)
    try:
        availticker = AvailableTickers.objects.all()
    except AvailableTickers.DoesNotExist:
        return JsonResponse({'message': 'Does not exist'}, status=status.HTTP_404_NOT_FOUND)

    if request.method == 'GET':
        return JsonResponse(serializers.serialize("json", availticker), safe=False)
    else:
        pass
