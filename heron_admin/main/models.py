from django.db import models
from django.conf import settings


class StateCoordinates(models.Model):
    STATECODE = models.CharField(max_length=150)
    LONGITUDE = models.FloatField(null=True)
    LATITUDE = models.FloatField(null=True)
    STATE = models.CharField(max_length=150)
    FIPS = models.CharField(max_length=150, default=None)


class WeatherData(models.Model):
    DATEFIELD = models.DateTimeField(default=None, null=True)
    HOURFIELD = models.IntegerField(default=None, null=True)
    TEMP = models.FloatField(null=True)
    UVI = models.FloatField(null=True)
    FEELSLIKE = models.FloatField(null=True)
    PRESSURE = models.FloatField(null=True)
    HUMIDITY = models.FloatField(null=True)
    DEWPOINT = models.FloatField(null=True)
    CLOUDS = models.FloatField(null=True)
    VISIBILITY = models.FloatField(null=True)
    WINDSPEED = models.FloatField(null=True)
    WINDGUST = models.FloatField(null=True)
    WINDDEGREE = models.FloatField(null=True)
    RAIN_1H = models.FloatField(null=True, default=None)
    RAIN_3H = models.FloatField(null=True, default=None)
    SNOW_1H = models.FloatField(null=True, default=None)
    SNOW_3H = models.FloatField(null=True, default=None)
    WEATHER_ID = models.CharField(max_length=150, default=None)
    WEATHER_MAIN = models.CharField(max_length=150, default=None)
    WEATHER_DESC = models.CharField(max_length=150, default=None)
    WEATHER_ICON = models.CharField(max_length=150, default=None)
    STATE = models.CharField(max_length=150)


class PortfolioPositions(models.Model):
    OrderId = models.CharField(max_length=150, default=None)
    Symbol = models.CharField(max_length=150, default=None)
    Shares = models.IntegerField(default=None, null=True)
    Value = models.DecimalField(
        max_digits=10, default=None, null=True, decimal_places=5)
    ComissionFees = models.DecimalField(max_digits=10, decimal_places=5)
    ExecutionPrice = models.DecimalField(max_digits=10, decimal_places=5)
    ExecutionDate = models.DateField(default=None, null=True)
    Position = models.CharField(max_length=150, default=None)
    Broker = models.CharField(max_length=150, default=None)
    FinancialInstrument = models.CharField(max_length=150, default=None)
    Currency = models.CharField(max_length=150, default=None)


class AvailableTickers(models.Model):
    Currency = models.CharField(max_length=150, default=None, null=True)
    Description = models.CharField(max_length=150, default=None, null=True)
    DisplaySymbol = models.CharField(max_length=150, default=None, null=True)
    FIGI = models.CharField(max_length=150, default=None, null=True)
    MIC = models.CharField(max_length=150, default=None, null=True)
    Symbol = models.CharField(max_length=150, default=None, null=True)
    Type = models.CharField(max_length=150, default=None, null=True)
