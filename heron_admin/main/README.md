# Algo Notepad

Point of this page is to plan out the architecture and logic we are using for the portfolio data.
Aim to maintain a low storage/memory overhead while displaying historical portfolio data on the portfolio front page.

## Structure

- We dont need to store stock by stock data, we can simply render the data out on request.
- We only need to store pre-computed portfolio data that is calculated from the historical stock data.

## Portfolio Performance Measures

We define the calculations and the corresponding variable names used for the different portfolio performance measures below.

### 1 Return Measures

**Portfolio Value (PV)** \
$V_{t}$ = $\sum_{i = 0}^{n} w_{i}P_{i}$ \
<sub> where i denotes the asset id, with n number of assets, at time t, </sub>
</br>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; <sub> w denotes the number of shares of asset i, at time t. </sub>
</br>

**Periodic Portfolio Returns (R)** \
We calculate the log continuous compounded returns, assuming that dividends and profits are reinvested.\
$R_{t}$ = $\frac{ log(V_{t}) }{ log(V_{t-1}) }$

**Summative Portfolio Returns (Rs)** \
When required to sum returns across different asset classes within the portfolio, we use simple returns instead, since Log Returns does not allow weighted sums of returns from each asset class. \
$Rs_{t}$ = $\frac{ V_{t} - V_{t-1} }{ V_{t-1} }$

### 2 Risk Measures

**Portfolio Optimisation** \
We use the [PyPortfolioOpt](https://pyportfolioopt.readthedocs.io/en/) implementation of the Black Litterman Model for our portfolio calculations. Quoted by the documentation, a useful source for understanding BLM is the step-by-step guide by Idzorek [here](https://faculty.fuqua.duke.edu/~charvey/Teaching/BA453_2006/Idzorek_onBL.pdf).

**What is the difference between Black-Litterman Optimisation and traditional Mean-Variance Optimisation objectives?**\
MVO is a subset of BLM Optimisation, with MVO still being included in one of the processes.
BLM simply takes into account the investor's risk tolerance and objective views, predictions of the assets within the portfolio i.e. It is forward looking.

**Our Implementation**

**Priors** \
Priors are the default calculations done as components to further calculations or as benchmark numbers. \
**Market Implied Risk Premium** \
$\delta$ = $\frac{ R - R_{f} }{ \sigma^{2} }$ \
**Market Implied Returns** \
$\Pi$ = $\delta⋅\sum w_{mkt}$

### Page References

<hr>
Math Formulae Markdown : https://rpruim.github.io/s341/S19/from-class/MathinRmd.html <br>
Return Calculations    : https://investmentcache.com/magic-of-log-returns-concept-part-1/
