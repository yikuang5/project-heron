import re
import json
import asyncio
import nest_asyncio
from datetime import datetime, timedelta

from ib_insync import IB, util
from ib_insync.contract import *

from django.contrib.auth import get_user_model
from channels.consumer import AsyncConsumer
from channels.db import database_sync_to_async

import time

import pandas as pd

# async def connectToIB():
#     ib = IB()
#     print("Starting IB")
#     ibConnectionPromise = await ib.connectAsync('127.0.0.1', 7497, 1)
#     return ibConnectionPromise

# async def reqRealTimeBars(ibConnectionPromise):
#     try:
#         ib = await ibConnectionPromise
#         nest_asyncio.apply()
#         reqRealTimeBars = ib.reqRealTimeBars(Forex('EURUSD'), 5, "MIDPOINT", True)
#         return reqRealTimeBars
#     except:
#         pass

# async def reqHistoricalData(ibConnectionPromise):
#     HistoricalData = await ibConnectionPromise.reqHistoricalData(Forex('EURUSD'), endDateTime='', durationStr='1 D', barSizeSetting='30 secs', whatToShow='MIDPOINT', useRTH=True)
#     return HistoricalData

# async def disconnect_to_ib(ibConnectionPromise):
#     ib = await ibConnectionPromise
#     ib.disconnect()

# def onReqBars(ticker, boolAvail):

#     try:
#         if boolAvail:
#             streamedData = pd.DataFrame(list(map(lambda x: (datetime.strptime(re.findall("....-..-.. ..:..:..", str(x.time + timedelta(hours = 8)))[0], "%Y-%m-%d %H:%M:%S"),
#                                                             x.open_, x.high, x.low, x.close),
#                                                             ticker)),
#                                                             columns = ["date","open","high","low","close"])
#         return streamedData

#     except Exception as e:
#         pass


class IbinsyncConsumer(AsyncConsumer):

    async def on_message(self, message):
        print(message)

    async def on_error(self, error):
        print(error)

    async def on_close(self):
        print("### closed ###")

    async def on_open(self):
        await self.send('{"type":"subscribe","symbol":"AAPL"}')
        await self.send('{"type":"subscribe","symbol":"AMZN"}')
        await self.send('{"type":"subscribe","symbol":"BINANCE:BTCUSDT"}')
        await self.send('{"type":"subscribe","symbol":"IC MARKETS:1"}')

    async def websocket_connect(self, event):

        websocket.enableTrace(True)
        ws = websocket.WebSocketApp("wss://ws.finnhub.io?token=c4uvoriad3id268asmjg",
                                    on_message=on_message,
                                    on_error=on_error,
                                    on_close=on_close)
        ws.on_open = on_open
        ws.run_forever()

        print("Connected", event)
        await self.send({
            "type": "websocket.accept"
        })

        await self.send({
            "type": "websocket.send",
            # For Full Historical Data str(await requestTick(IBKR))
            'text': "Hello"
        })

    async def websocket_disconnect(self, event):
        print('Disconnected', event)
