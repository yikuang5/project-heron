from django.contrib import admin
from .models import StateCoordinates, WeatherData, PortfolioPositions, AvailableTickers

# Register your models here.
admin.site.register(StateCoordinates)
admin.site.register(WeatherData)
admin.site.register(PortfolioPositions)
admin.site.register(AvailableTickers)
