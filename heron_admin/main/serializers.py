from rest_framework import serializers
from .models import PortfolioPositions, AvailableTickers


class PortfolioPositionsSerializer(serializers.ModelSerializer):
    class Meta:
        model = PortfolioPositions
        fields = ["OrderId", "Symbol", "Shares", "Value", "ComissionFees", "ExecutionPrice",
                  "ExecutionDate", "Position", "FinancialInstrument", "Currency", "Broker"]


class PortfolioOvPosSerializer(serializers.Serializer):
    Symbol = serializers.CharField()
    FinancialInstrument = serializers.CharField()
    OverallPosition = serializers.IntegerField()


class AvailableTickerSerializer(serializers.ModelSerializer):
    class Meta:
        model = AvailableTickers
        fields = ["Currency", "Description",
                  "DisplaySymbol", "FIGI", "MIC", "Symbol", "Type"]
