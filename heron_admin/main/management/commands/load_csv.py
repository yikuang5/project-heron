import csv
import time
from main.models import AvailableTickers
from django.core.management.base import BaseCommand

class Command(BaseCommand):
    help = "Loads products and product categories from CSV file."

    def add_arguments(self, parser):
        parser.add_argument("file_path", type=str)

    def handle(self, *args, **options):

        start_time = time.time()
        file_path = options["file_path"]

        with open(file_path, "r") as csv_file:
            data = csv.reader(csv_file, delimiter=",")
            product_categories = {p_category.code: p_category for p_category in AvailableTickers.objects.all()}
            products = []

            for row in data:
                items = AvailableTickers(
                    Currency = row[0] if row[0] else None,
                    Description = row[1] if row[1] else None,
                    DisplaySymbol = row[2] if row[2] else None,
                    FIGI = row[3] if row[3] else None,
                    MIC = row[4] if row[4] else None,
                    Symbol = row[5] if row[5] else None,
                    Type = row[6] if row[6] else None,
                )
                
                products.append(items)

                if len(products) > 5000:
                    AvailableTickers.objects.bulk_create(products)
                    products = []
            if products:
                AvailableTickers.objects.bulk_create(products)

        end_time = time.time()

        self.stdout.write(
            self.style.SUCCESS(
                f"Loading CSV took: {end_time - start_time} seconds."
            )
        )
