<p align="center">
    <img src="media/Heron.png" width="200"/>
</p>
<h1 align="center">
  Heron: Portfolio/Quant Trading GUI 
</h1>

Quantitative Trading Portfolio GUI for IBKR (API is portable)

A simple quantitative dashboard with a hook-up for the IBKR API. Research data can be read from [here](https://www.agricensus.com/corn-news/).\
Restricted reference to our planning and production slides can be found [here](https://docs.google.com/presentation/d/1cl2bjh46tSXF35if7Lq7QtPmHU_CE2gDxegekS_-Q1U/edit#slide=id.ge446589231_0_112).

## Project Contributors

<p float = "left">
  <a href="https://www.linkedin.com/in/lohyikuang/"><img src="media/project_contributors/yik.png" width="75" height = "auto" style="border-radius:50%"/></a>
  <a href="https://www.linkedin.com/in/chayanit-wanwanich-a46a88187/"><img src="media/project_contributors/nid.png" width="75" height = "auto" style="border-radius:50%"/></a>
</p>

## 🚀 Quick start

#### 1. **Install required packages **

```bash
pip3 install -r requirements.txt
```

#### 2. **Make sure Trader Work Station (TWS) is Open**

Port 7497 for paper trading, 7496 for real-time trading. Make sure algorithms work before plugging into real-time data API.

#### 3. **Run on Local Host**

```bash
cd heron_admin && sh heronrun.sh
```

#### 4. **Kill Heron**

TAKE NOTE: This command kills all running processes listening on port 3000 and 8000

```bash
sh heronkill.sh
```

#### Code Bases Used
**Code Formatting**
.prettierrc for JS Codebase, autopep8 for Python