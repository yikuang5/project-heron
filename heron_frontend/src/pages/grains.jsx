import React from 'react'

import AgencyCalendar from '../components/grains/agencycalendar'
import ProgressCondition from '../components/grains/progresscondition'
import ReportTables from '../components/grains/reporttables'
import LiveNews from '../components/grains/livenews'

import Grid from '@material-ui/core/Grid'
import Header from '../components/header'

import grainsStyles from '../styles/grains.module.css'
import { StylesProvider } from '@material-ui/core/styles'

const Grains = () => {
  return (
    <>
      <Header />
      <Grid container>
        <div className={grainsStyles.all}>
          <div className={grainsStyles.div0}>
            <div className={grainsStyles.agencycalendar}>
              <StylesProvider injectFirst>
                <AgencyCalendar />
              </StylesProvider>
            </div>
          </div>

          <div className={grainsStyles.div1}>
            <div className={grainsStyles.progresscondition}>
              <h3 className={grainsStyles.pcheaders}>
                {' '}
                Corn Crop Progress and Conditions for Year 2021{' '}
              </h3>
              <ProgressCondition />
            </div>
          </div>

          <div className={grainsStyles.div2}>
            <div className={grainsStyles.reporttables}>
              <StylesProvider injectFirst>
                <ReportTables />
              </StylesProvider>
            </div>
            <div className={grainsStyles.livenews}>
              <StylesProvider injectFirst>
                <LiveNews />
              </StylesProvider>
            </div>
          </div>
        </div>
      </Grid>
    </>
  )
}

export default Grains
