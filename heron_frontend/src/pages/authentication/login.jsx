import React, { useState } from 'react'
import axiosInstance from '../../APIentrypoints'
import { useHistory } from 'react-router-dom'

import Heron from '../../static/images/Heron.png'

import Button from '@material-ui/core/Button'
import TextField from '@material-ui/core/TextField'
import Link from '@material-ui/core/Link'
import Grid from '@material-ui/core/Grid'
import Typography from '@material-ui/core/Typography'
import { withStyles, makeStyles } from '@material-ui/core/styles'
import Container from '@material-ui/core/Container'

const CssTextField = withStyles({
  root: {
    '& label.Mui-focused': {
      color: '#D3D3D3'
    },
    '& label': {
      color: '#BBBBBB'
    },
    '& .MuiInput-underline:after': {
      borderBottomColor: '#DFDFDF'
    },
    '& .MuiInput-underline': {
      borderBottomColor: '#DFDFDF'
    },
    '& .MuiOutlinedInput-root': {
      '& fieldset': {
        borderColor: '#C0C0C0'
      },
      '&:hover fieldset': {
        borderColor: '#DCDCDC'
      },
      '&.Mui-focused fieldset': {
        borderColor: '#DFDFDF'
      }
    }
  }
})(TextField)

const useStyles = makeStyles(theme => ({
  root: {
    background: 'transparent',
    color: 'white'
  },
  hideinputbg: {
    backgroundColor: 'transparent'
  },
  input: {
    color: 'white',
    paddingLeft: '5px',
    background: 'transparent',
    borderBottom: '0.5px solid #BBBABA'
  },
  paper: {
    marginTop: theme.spacing(8),
    flexDirection: 'column',
    alignItems: 'center',
    color: 'white'
  },
  heronimg: {
    width: '100%',
    marginRight: '15%',
    marginBottom: '10%'
  },
  form: {
    width: '100%', // Fix IE 11 issue.
    marginTop: theme.spacing(1)
  },
  submit: {
    margin: theme.spacing(3, 0, 2)
  }
}))

export default function SignIn() {
  const history = useHistory()
  const initialFormData = Object.freeze({
    email: '',
    password: ''
  })

  const [formData, updateFormData] = useState(initialFormData)

  const handleChange = e => {
    updateFormData({
      ...formData,
      [e.target.name]: e.target.value.trim()
    })
  }

  const handleSubmit = e => {
    e.preventDefault()
    axiosInstance
      .post(`token/`, {
        email: formData.email,
        password: formData.password
      })
      .then(res => {
        localStorage.setItem('access_token', res.data.access)
        localStorage.setItem('refresh_token', res.data.refresh) // Get the refresh and access token
        axiosInstance.defaults.headers['Authorization'] =
          'JWT ' + localStorage.getItem('access_token')
        history.push('/')
      })
  }

  const classes = useStyles()

  return (
    <Container component="main" maxWidth="xs">
      <Grid container className={classes.paper}>
        <img src={Heron} className={classes.heronimg} />
        <Typography component="h1" variant="h5">
          Sign in
        </Typography>
        <form className={classes.form} noValidate>
          <Grid item>
            <CssTextField
              variant="standard"
              id="email"
              label="Email Address"
              className={classes.textfield}
              InputProps={{ className: classes.input }}
              margin="normal"
              required
              fullWidth
              name="email"
              autoComplete="none"
              onChange={handleChange}
            />
          </Grid>
          <Grid item>
            <CssTextField
              variant="standard"
              margin="normal"
              required
              fullWidth
              name="password"
              label="Password"
              className={classes.textfield}
              InputProps={{ className: classes.input }}
              type="password"
              id="password"
              autoComplete="none"
              onChange={handleChange}
            />
          </Grid>
          <Grid item>
            <Button
              type="submit"
              fullWidth
              variant="contained"
              color="primary"
              className={classes.submit}
              onClick={handleSubmit}
            >
              Sign In
            </Button>
          </Grid>
          <Grid container>
            <Grid item>
              <Link href="/register" variant="body2">
                {"Don't have an account? Sign Up"}
              </Link>
            </Grid>
          </Grid>
        </form>
      </Grid>
    </Container>
  )
}
