import React from 'react'

import CircularProgress from '@material-ui/core/CircularProgress'
import Grid from '@material-ui/core/Grid'
import { Typography } from '@material-ui/core'

const LoadLogout = props => {
  return (
    <Grid
      spacing={0}
      direction="column"
      alignItems="center"
      justifyContent="center"
      style={{ minHeight: '100vh' }}
    >
      <CircularProgress />
      <Typography> {props.text} </Typography>
    </Grid>
  )
}
export default LoadLogout
