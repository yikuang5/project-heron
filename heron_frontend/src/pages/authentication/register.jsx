import React, { useState } from 'react'

import axiosInstance from '../../APIentrypoints'
import { useHistory } from 'react-router-dom'

import Heron from '../../static/images/Heron.png'

import Button from '@material-ui/core/Button'
import TextField from '@material-ui/core/TextField'
import Link from '@material-ui/core/Link'
import Grid from '@material-ui/core/Grid'
import Typography from '@material-ui/core/Typography'
import { withStyles, makeStyles } from '@material-ui/core/styles'
import Container from '@material-ui/core/Container'

const CssTextField = withStyles({
  root: {
    '& label.Mui-focused': {
      color: '#D3D3D3'
    },
    '& label': {
      color: '#BBBBBB'
    },
    '& .MuiInput-underline:after': {
      borderBottomColor: '#DFDFDF'
    },
    '& .MuiInput-underline': {
      borderBottomColor: '#DFDFDF'
    },
    '& .MuiOutlinedInput-root': {
      '& fieldset': {
        borderColor: '#C0C0C0'
      },
      '&:hover fieldset': {
        borderColor: '#DCDCDC'
      },
      '&.Mui-focused fieldset': {
        borderColor: '#DFDFDF'
      }
    }
  }
})(TextField)

const useStyles = makeStyles(theme => ({
  root: {
    background: 'transparent',
    color: 'white'
  },
  hideinputbg: {
    backgroundColor: 'transparent'
  },
  input: {
    color: 'white',
    paddingLeft: '5px',
    background: 'transparent',
    borderBottom: '0.5px solid #BBBABA'
  },
  paper: {
    marginTop: theme.spacing(5),
    flexDirection: 'column',
    alignItems: 'center',
    color: 'white'
  },
  heronimg: {
    width: '100%',
    marginRight: '15%',
    marginBottom: '10%'
  },
  form: {
    width: '100%', // Fix IE 11 issue.
    marginTop: theme.spacing(4)
  },
  submit: {
    margin: theme.spacing(3, 0, 2)
  }
}))

export default function SignUp() {
  const history = useHistory()
  const initialFormData = Object.freeze({
    email: '',
    username: '',
    password: ''
  })

  const [formData, updateFormData] = useState(initialFormData)

  const handleChange = e => {
    updateFormData({
      ...formData,
      // Trimming any whitespace
      [e.target.name]: e.target.value.trim()
    })
  }

  const handleSubmit = e => {
    e.preventDefault()
    console.log(formData)

    axiosInstance
      .post(`user/create/`, {
        email: formData.email,
        user_name: formData.username,
        password: formData.password
      })
      .then(res => {
        history.push('/login')
        console.log(res)
        console.log(res.data)
      })
  }

  const classes = useStyles()

  return (
    <Container component="main" maxWidth="xs">
      <Grid container className={classes.paper}>
        <img src={Heron} className={classes.heronimg} />
        <Typography component="h1" variant="h5">
          Sign up
        </Typography>
        <form className={classes.form} noValidate>
          <Grid item xs={12} spacing={200}>
            <CssTextField
              variant="standard"
              label="Email Address"
              id="email"
              required
              fullWidth
              className={classes.textfield}
              InputProps={{ className: classes.input }}
              name="email"
              autoComplete="none"
              onChange={handleChange}
            />
          </Grid>
          <Grid item xs={12} spacing={3}>
            <CssTextField
              variant="standard"
              required
              fullWidth
              className={classes.textfield}
              InputProps={{ className: classes.input }}
              id="username"
              label="Username"
              name="username"
              autoComplete="none"
              onChange={handleChange}
            />
          </Grid>
          <Grid item>
            <CssTextField
              variant="standard"
              required
              fullWidth
              className={classes.textfield}
              InputProps={{ className: classes.input }}
              name="password"
              label="Password"
              type="password"
              id="password"
              autoComplete="none"
              onChange={handleChange}
            />
          </Grid>
          <Button
            type="submit"
            fullWidth
            variant="contained"
            color="primary"
            className={classes.submit}
            onClick={handleSubmit}
          >
            Sign Up
          </Button>
          <Grid container justify="flex-end">
            <Grid item>
              <Link href="/login" variant="body2">
                Already have an account? Sign in
              </Link>
            </Grid>
          </Grid>
        </form>
      </Grid>
    </Container>
  )
}
