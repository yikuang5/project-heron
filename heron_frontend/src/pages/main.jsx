import { React, useState, useEffect } from 'react'
import MainStyles from '../styles/main.module.css'

import PortfolioTable from '../components/main/portfoliotable'
import PieChart from '../components/main/piechart'
import LineChart from '../components/main/linechart'
import PerfTable from '../components/main/perftable'
import RunSimul from '../components/main/runsimulations'

import Grid from '@material-ui/core/Grid'
import Header from '../components/header'

import dotenv from 'dotenv'
import axios from 'axios'
export const baseUrl = 'http://localhost:8000/api'

dotenv.config()

try {
  // .env file containing the react api key must be located at the root of the frontend directory
  var socket = new WebSocket('wss://ws.finnhub.io?token=' + process.env.REACT_APP_FINNHUBAPIKEY)

  // Connection opened -> Subscribe
  socket.addEventListener('open', function (event) {
    socket.send(JSON.stringify({ type: 'subscribe', symbol: 'AAPL' }))
    socket.send(JSON.stringify({ type: 'subscribe', symbol: 'BINANCE:BTCUSDT' }))
    socket.send(JSON.stringify({ type: 'subscribe', symbol: 'BINANCE:ADAUSDT' }))
  })

  // Unsubscribe
  // var unsubscribe = function(symbol) {
  //   socket.send(JSON.stringify({'type':'unsubscribe','symbol': symbol}))
  // }

  socket.addEventListener('disconnect', () => {
    socket.removeAllListeners()
  })
} catch {
  console.log('Websocket connection failed')
}

// socket.close()

const Main = () => {
  var initialValue = []
  const [data, setData] = useState(initialValue)
  const [portfolio, setPortfolio] = useState([])

  var root = {
    name: 'Assets',
    color: '#212529',
    children: [
      {
        name: 'Futures',
        color: '#84acce',
        key: '',
        children: [
          { name: 'cluster', size: 5000 },
          { name: 'graph', size: 8045 },
          { name: 'optimization', size: 7074 }
        ]
      },
      {
        name: 'ETFs',
        color: '#717744',
        key: '',
        children: [
          { name: 'Easing', size: 17010 },
          { name: 'Tween', size: 6006 }
        ]
      },
      {
        name: 'Stocks',
        color: '#ff8484',
        key: '',
        children: [
          { name: 'DirtySprite', size: 8833 },
          { name: 'LineSprite', size: 1732 }
        ]
      },
      {
        name: 'Options',
        color: '#f6ae2d',
        key: '',
        children: [
          { name: 'Arrays', size: 8258 },
          { name: 'Stats', size: 6557 }
        ]
      },
      {
        name: 'Cryptocurrency',
        color: '#6247aa',
        key: '',
        children: [
          { name: 'operator', size: 10000 },
          { name: 'Visualization', size: 16540 }
        ]
      }
    ]
  }

  useEffect(() => {
    axios
      .get(baseUrl + '/portfolioposition')
      .then(response => {
        setPortfolio(response.data)
      })
      .catch(e => {
        console.warn('Error while getting portfolio details ... ...')
      })
    try {
      socket.addEventListener('message', function (event) {
        setData(oldArray => [].concat(...oldArray, JSON.parse(event.data)['data']).slice(-80)) // Get the latest 50 rows of data
      })
      return () => console.log('Disconnected')
    } catch {
      console.log('Websocket Connection failed')
    }
  }, [])

  return (
    <>
      <Header />
      <Grid container className={MainStyles.main}>
        <Grid item xs={6}>
          <Grid>
            <h3 className={MainStyles.lcHeader}> Portfolio Performance </h3>
            <LineChart />
            <RunSimul />
          </Grid>
        </Grid>

        <Grid item xs={6}>
          <Grid container spacing={3}>
            <Grid item xs={12} sm={6}>
              <PieChart root={root} />
            </Grid>
            <Grid item xs={12} sm={6}>
              <h4 className={MainStyles.PortfolioHeader}> Portfolio Holdings </h4>
              <Grid className={MainStyles.portfolioTable}>
                <PortfolioTable data={data} portfolio={portfolio} />
              </Grid>
            </Grid>
            <Grid item xs={12}>
              <PerfTable />
            </Grid>
          </Grid>
        </Grid>
      </Grid>
    </>
  )
}
export default Main
