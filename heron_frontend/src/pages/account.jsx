import { React } from 'react'
import AccountStyles from '../styles/account.module.css'
import PortfolioAdditor from '../components/account/portfolioadditor'

import Grid from '@material-ui/core/Grid'

import Header from '../components/header'

import dotenv from 'dotenv'

dotenv.config()

const Account = () => {
  return (
    <>
      <Header />
      <Grid container className={AccountStyles.main}>
        <Grid item xs={7}>
          <Grid className={AccountStyles.portfolioContainer}>
            <h4 className={AccountStyles.TranscHist}> Transaction History </h4>
            <PortfolioAdditor />
          </Grid>
        </Grid>

        <Grid item xs={5}></Grid>
      </Grid>
    </>
  )
}
export default Account
