import React from 'react'

import { BrowserRouter as Router, Route, useHistory, useLocation } from 'react-router-dom'
import PrivateRouter from './auth/privaterouter'

import Main from './pages/main'
import Grain from './pages/grains'
import Account from './pages/account'
import Register from './pages/authentication/register'
import Login from './pages/authentication/login'
import Logout from './pages/authentication/logout'
import './App.css'

const App = () => {
  return (
    <Router forceRefresh={true}>
      <PrivateRouter exact path="/" component={Main} />
      <Route exact path="/register" component={Register} />
      <Route exact path="/login" component={Login} />
      <PrivateRouter exact path="/logout" component={Logout} />
      <PrivateRouter exact path="/grains" component={Grain} />
      <PrivateRouter exact path="/account" component={Account} />
    </Router>
  )
}
export default App
