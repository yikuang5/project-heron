import React from 'react'
import perftableStyles from '../../styles/mainStyles/perftable.module.css'

import PropTypes from 'prop-types'
import Box from '@material-ui/core/Box'
import Collapse from '@material-ui/core/Collapse'
import IconButton from '@material-ui/core/IconButton'
import Table from '@material-ui/core/Table'
import TableBody from '@material-ui/core/TableBody'
import TableCell from '@material-ui/core/TableCell'
import TableContainer from '@material-ui/core/TableContainer'
import TableHead from '@material-ui/core/TableHead'
import TableRow from '@material-ui/core/TableRow'
import Typography from '@material-ui/core/Typography'
import KeyboardArrowDownIcon from '@material-ui/icons/KeyboardArrowDown'
import KeyboardArrowUpIcon from '@material-ui/icons/KeyboardArrowUp'
import { StylesProvider } from '@material-ui/core/styles'

function createData(name, calories, fat, carbs, protein, price) {
  return {
    name,
    calories,
    fat,
    carbs,
    protein,
    price,
    history: [
      { date: '2020-01-05', customerId: '11091700', amount: 3 },
      { date: '2020-01-02', customerId: 'Anonymous', amount: 1 }
    ]
  }
}

function Row(props) {
  const { row } = props
  const [open, setOpen] = React.useState(false)

  return (
    <React.Fragment>
      <StylesProvider injectFirst>
        <TableRow>
          <TableCell className={perftableStyles.PerfTableCell}>
            <IconButton aria-label="expand row" size="small" onClick={() => setOpen(!open)}>
              {open ? (
                <KeyboardArrowUpIcon style={{ fill: 'white' }} />
              ) : (
                <KeyboardArrowDownIcon style={{ fill: 'white' }} />
              )}
            </IconButton>
          </TableCell>
          <TableCell className={perftableStyles.PerfTableCell} component="th" scope="row">
            {row.name}
          </TableCell>
          <TableCell className={perftableStyles.PerfTableCell} align="right">
            {row.calories}
          </TableCell>
          <TableCell className={perftableStyles.PerfTableCell} align="right">
            {row.fat}
          </TableCell>
          <TableCell className={perftableStyles.PerfTableCell} align="right">
            {row.carbs}
          </TableCell>
          <TableCell className={perftableStyles.PerfTableCell} align="right">
            {row.protein}
          </TableCell>
        </TableRow>

        <TableRow>
          <TableCell style={{ paddingBottom: 0, paddingTop: 0, border: 'none' }} colSpan={6}>
            <Collapse in={open} timeout="auto" unmountOnExit>
              <Box margin={0.5}>
                <Typography
                  variant="h6"
                  gutterBottom
                  component="div"
                  className={perftableStyles.PerfTableTitle}
                >
                  History
                </Typography>
                <Table size="small" aria-label="purchases" style={{ border: 'none' }}>
                  <TableHead>
                    <TableRow>
                      <TableCell className={perftableStyles.PerfTableMainHeader}>Date</TableCell>
                      <TableCell className={perftableStyles.PerfTableMainHeader}>
                        Customer
                      </TableCell>
                      <TableCell className={perftableStyles.PerfTableMainHeader} align="right">
                        Amount
                      </TableCell>
                      <TableCell className={perftableStyles.PerfTableMainHeader} align="right">
                        Total price ($)
                      </TableCell>
                    </TableRow>
                  </TableHead>
                  <TableBody style={{ border: 'none' }}>
                    {row.history.map(historyRow => (
                      <TableRow key={historyRow.date}>
                        <TableCell
                          component="th"
                          scope="row"
                          className={perftableStyles.PerfTableCell}
                        >
                          {historyRow.date}
                        </TableCell>
                        <TableCell className={perftableStyles.PerfTableCell}>
                          {historyRow.customerId}
                        </TableCell>
                        <TableCell className={perftableStyles.PerfTableCell} align="right">
                          {historyRow.amount}
                        </TableCell>
                        <TableCell className={perftableStyles.PerfTableCell} align="right">
                          {Math.round(historyRow.amount * row.price * 100) / 100}
                        </TableCell>
                      </TableRow>
                    ))}
                  </TableBody>
                </Table>
              </Box>
            </Collapse>
          </TableCell>
        </TableRow>
      </StylesProvider>
    </React.Fragment>
  )
}

const rows = [
  createData('Frozen yoghurt', 159, 6.0, 24, 4.0, 3.99),
  createData('Ice cream sandwich', 237, 9.0, 37, 4.3, 4.99),
  createData('Eclair', 262, 16.0, 24, 6.0, 3.79),
  createData('Cupcake', 305, 3.7, 67, 4.3, 2.5),
  createData('Gingerbread', 356, 16.0, 49, 3.9, 1.5)
]

export default function PerfTable() {
  return (
    <TableContainer className={perftableStyles.TableContainer}>
      <Table aria-label="collapsible table">
        <TableHead>
          <TableRow>
            <TableCell />
            <TableCell className={perftableStyles.PerfTableHeader}>
              Dessert (100g serving)
            </TableCell>
            <TableCell className={perftableStyles.PerfTableHeader} align="right">
              Calories
            </TableCell>
            <TableCell className={perftableStyles.PerfTableHeader} align="right">
              Fat&nbsp;(g)
            </TableCell>
            <TableCell className={perftableStyles.PerfTableHeader} align="right">
              Carbs&nbsp;(g)
            </TableCell>
            <TableCell className={perftableStyles.PerfTableHeader} align="right">
              Protein&nbsp;(g)
            </TableCell>
          </TableRow>
        </TableHead>
        <TableBody>
          {rows.map(row => (
            <Row key={row.name} row={row} />
          ))}
        </TableBody>
      </Table>
    </TableContainer>
  )
}
