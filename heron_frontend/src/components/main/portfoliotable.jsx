import React from 'react'

import MainStyles from '../../styles/main.module.css'

import Table from '@material-ui/core/Table'
import TableBody from '@material-ui/core/TableBody'
import TableCell from '@material-ui/core/TableCell'
import TableContainer from '@material-ui/core/TableContainer'
import TableHead from '@material-ui/core/TableHead'
import TableRow from '@material-ui/core/TableRow'
import { StylesProvider } from '@material-ui/core/styles'

export const baseUrl = 'http://localhost:8000/api'
export const headers = {
  'Content-type': 'application/json'
}

class portfolioTable extends React.Component {
  constructor(props) {
    super(props)
    this.state = {
      items: []
    }
  }

  getSymbolPrice(s) {
    try {
      var ticks = this.props.data
        .slice()
        .reverse()
        .find(item => item.s === s)
      if (ticks.p !== undefined && ticks.p !== 'undefined' && ticks.p !== null && ticks.p !== '') {
        return ticks.p
      }
    } catch {}
  }

  render() {
    return (
      <>
        <div className={MainStyles.portfolioBox}>
          <StylesProvider injectFirst>
            <TableContainer>
              <Table
                aria-label="collapsible table"
                stickyHeader
                className={MainStyles.tickerPortfolioTable}
              >
                <TableHead>
                  <TableRow key={'headers'}>
                    <TableCell className={MainStyles.tickerPortfolioHead}> Cls </TableCell>
                    <TableCell className={MainStyles.tickerPortfolioHead}> Symbol </TableCell>
                    <TableCell className={MainStyles.tickerPortfolioHead}> Price </TableCell>
                    <TableCell className={MainStyles.tickerPortfolioHead}> Value </TableCell>
                    <TableCell className={MainStyles.tickerPortfolioHead}> P&L Day </TableCell>
                    <TableCell className={MainStyles.tickerPortfolioHead}> P&L </TableCell>
                    <TableCell className={MainStyles.tickerPortfolioHead}> Pred </TableCell>
                  </TableRow>
                </TableHead>
                <TableBody>
                  {this.props.portfolio.map((row, i) => (
                    <TableRow key={`row-${i}`}>
                      <TableCell className={MainStyles.tickerPortfolioCell}>
                        {' '}
                        {(() => {
                          if (row.OverallPosition > 0) {
                            return (
                              '+' +
                              row.FinancialInstrument.slice(0, 1) +
                              Math.abs(row.OverallPosition)
                            )
                          } else {
                            return (
                              '-' +
                              row.FinancialInstrument.slice(0, 1) +
                              Math.abs(row.OverallPosition)
                            )
                          }
                        })()}{' '}
                        <sup className={MainStyles.supcurr}> {row.Currency} </sup>
                      </TableCell>
                      <TableCell className={MainStyles.tickerPortfolioCell}>
                        {' '}
                        {row.Symbol} <span className={MainStyles.dot}></span>{' '}
                      </TableCell>
                      <TableCell className={MainStyles.tickerPortfolioCell}>
                        {' '}
                        ${this.getSymbolPrice(row.Symbol)}{' '}
                      </TableCell>
                      <TableCell className={MainStyles.tickerPortfolioCell}> Faux </TableCell>
                      <TableCell className={MainStyles.tickerPortfolioCell}> Faux </TableCell>
                      <TableCell className={MainStyles.tickerPortfolioCell}> </TableCell>
                      <TableCell className={MainStyles.tickerPortfolioCell}>
                        {' '}
                        <span className={MainStyles.dot}></span>{' '}
                      </TableCell>
                    </TableRow>
                  ))}
                </TableBody>
              </Table>
            </TableContainer>
          </StylesProvider>
        </div>
      </>
    )
  }
}

export default portfolioTable
