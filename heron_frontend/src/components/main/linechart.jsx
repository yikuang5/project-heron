import { React, useRef, useEffect } from 'react'
import * as d3 from 'd3v4'
import MainStyles from '../../styles/main.module.css'

const LineChart = () => {
  const LineChartRef = useRef()

  useEffect(() => {
    // set the dimensions and margins of the graph
    var margin = { top: 10, right: 60, bottom: 30, left: 30 }
    var width = 600 - margin.left - margin.right + 50
    var height = 450 - margin.top - margin.bottom

    // append the svg object to the body of the page
    var svg = d3
      .select(LineChartRef.current)
      .append('svg')
      .attr('preserveAspectRatio', 'xMinYMin meet')
      .attr(
        'viewBox',
        '0 0 ' + (width + margin.left + margin.right) + ' ' + (height + margin.top + margin.bottom)
      )
      .append('g')
      .attr('transform', 'translate(' + margin.left + ',' + margin.top + ')')

    //Read the data
    d3.csv(
      'https://raw.githubusercontent.com/holtzy/data_to_viz/master/Example_dataset/3_TwoNumOrdered_comma.csv',

      // When reading the csv, I must format variables:
      function (d) {
        return { date: d3.timeParse('%Y-%m-%d')(d.date), value: d.value }
      },

      // Now I can use this dataset:
      function (data) {
        // Add X axis --> it is a date format
        var x = d3
          .scaleTime()
          .domain(
            d3.extent(data, function (d) {
              return d.date
            })
          )
          .range([0, width])

        var xAxis = svg
          .append('g')
          .attr('transform', 'translate(0,' + height + ')')
          .attr('color', 'white')
          .attr('stroke', 'white')
          .attr('stroke-width', 1)
          .attr('opacity', 0.7)
          .call(d3.axisBottom(x).tickSizeOuter(0).tickSizeInner(0).tickSize(0))
          .call(g => g.select('.domain').remove())

        // Add Y axis
        var y = d3
          .scaleLinear()
          .domain([
            0,
            d3.max(data, function (d) {
              return +d.value
            })
          ])
          .range([height, 0])

        var yAxis = svg
          .append('g')
          .attr('transform', 'translate( ' + (width + 10) + ', 0 )')
          .attr('color', 'white')
          .attr('stroke', 'white')
          .attr('stroke-width', 1)
          .attr('opacity', 0.7)
          .call(d3.axisRight(y).tickSizeOuter(0).tickSizeInner(0).tickSize(0))
          .call(g => g.select('.domain').remove())

        // This allows to find the closest X index of the mouse:
        var bisect = d3.bisector(function (d) {
          return d.date
        }).left

        function mouseover() {
          focus.style('opacity', 1).style('fill', 'white')
          focusText.style('opacity', 1).style('fill', 'white')
        }

        function mousemove() {
          // recover coordinate we need
          var x0 = x.invert(d3.mouse(this)[0])
          var i = bisect(data, x0, i)
          var selectedData = data[i]
          focus.attr('cx', x(selectedData.date)).attr('cy', y(selectedData.value))
          focusText
            .html(
              "<tspan x='0' dy='1.5em'> Date: " +
                selectedData.date.toLocaleDateString() +
                "</tspan><tspan x='0' dy='1.5em'> Value: " +
                selectedData.value +
                '</tspan>'
            )
            .attr('x', 0)
            .attr('y', 0)
        }
        function mouseout() {
          focus.style('opacity', 0)
          focusText.style('opacity', 0)
        }

        // Create the circle that travels along the curve of chart
        var focus = svg
          .append('g')
          .append('circle')
          .style('fill', 'none')
          .attr('stroke', 'white')
          .attr('r', 4)
          .style('opacity', 0)

        // Create the text that travels along the curve of chart
        var focusText = svg
          .append('g')
          .append('text')
          .style('opacity', 0)
          .style('font-size', 10)
          .attr('text-anchor', 'left')
          .attr('alignment-baseline', 'middle')

        // Add a clipPath: everything out of this area won't be drawn.
        var clip = svg
          .append('defs')
          .append('svg:clipPath')
          .attr('id', 'clip')
          .append('svg:rect')
          .attr('width', width)
          .attr('height', height)
          .attr('x', 0)
          .attr('y', 0)

        // Add brushing
        var brush = d3
          .brushX() // Add the brush feature using the d3.brush function
          .extent([
            [0, 0],
            [width, height]
          ]) // initialise the brush area: start at 0,0 and finishes at width,height: it means I select the whole graph area
          .on('end', updateChart) // Each time the brush selection changes, trigger the 'updateChart' function

        // Create the line variable: where both the line and the brush take place
        var line = svg.append('g').attr('clip-path', 'url(#clip)')

        // Add the line
        line
          .append('path')
          .datum(data)
          .attr('class', 'line') // I add the class line to be able to modify this line later on.
          .attr('fill', 'none')
          .attr('stroke', '#00BFFF')
          .attr('stroke-width', 1.5)
          .attr(
            'd',
            d3
              .line()
              .x(function (d) {
                return x(d.date)
              })
              .y(function (d) {
                return y(d.value)
              })
          )

        // Add the brushing
        line
          .append('g')
          .attr('class', 'brush')
          .call(brush)
          .style('pointer-events', 'all')
          .attr('width', width)
          .attr('height', height)
          .on('mouseover', mouseover)
          .on('mousemove', mousemove)
          .on('mouseout', mouseout)

        // A function that set idleTimeOut to null
        var idleTimeout
        function idled() {
          idleTimeout = null
        }

        // A function that update the chart for given boundaries
        function updateChart() {
          // What are the selected boundaries?
          var extent = d3.event.selection

          // If no selection, back to initial coordinate. Otherwise, update X axis domain
          if (!extent) {
            if (!idleTimeout) return (idleTimeout = setTimeout(idled, 350)) // This allows to wait a little bit
            x.domain([4, 8])
          } else {
            x.domain([x.invert(extent[0]), x.invert(extent[1])])
            line.select('.brush').call(brush.move, null) // This remove the grey brush area as soon as the selection has been done
          }

          // Update axis and line position
          xAxis.transition().duration(1000).call(d3.axisBottom(x))
          line
            .select('.line')
            .transition()
            .duration(1000)
            .attr(
              'd',
              d3
                .line()
                .x(function (d) {
                  return x(d.date)
                })
                .y(function (d) {
                  return y(d.value)
                })
            )
        }

        // If user double click, reinitialize the chart
        svg.on('dblclick', function () {
          x.domain(
            d3.extent(data, function (d) {
              return d.date
            })
          )
          xAxis.transition().call(d3.axisBottom(x))
          line
            .select('.line')
            .transition()
            .attr(
              'd',
              d3
                .line()
                .x(function (d) {
                  return x(d.date)
                })
                .y(function (d) {
                  return y(d.value)
                })
            )
        })
      }
    )
  }, [])

  return (
    <div>
      <div ref={LineChartRef} className={MainStyles.LineChart}></div>
    </div>
  )
}

export default LineChart
