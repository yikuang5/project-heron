import React from 'react'
import runsimulStyles from '../../styles/mainStyles/runsimul.module.css'

class RunSimulations extends React.Component {
  render() {
    return (
      <>
        <h2 className={runsimulStyles.header}> Run Simulations </h2>
      </>
    )
  }
}

export default RunSimulations
