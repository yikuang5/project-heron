import { React, useRef, useEffect } from 'react'
import * as d3 from 'd3v4'

function PieChart(props) {
  const PieChartRef = useRef()

  useEffect(() => {
    var width = 760 * (2 / 3)
    var height = 500 * (2 / 3)
    var radius = Math.min(width, height) / 2 - 10

    var formatNumber = d3.format(',d')

    var x = d3.scaleLinear().range([0, 2 * Math.PI])

    var y = d3.scaleSqrt().range([0, radius])

    var partition = d3.partition()

    var arc = d3
      .arc()
      .startAngle(function (d) {
        return Math.max(0, Math.min(2 * Math.PI, x(d.x0)))
      })
      .endAngle(function (d) {
        return Math.max(0, Math.min(2 * Math.PI, x(d.x1)))
      })
      .innerRadius(function (d) {
        return Math.max(0, y(d.y0))
      })
      .outerRadius(function (d) {
        return Math.max(0, y(d.y1))
      })

    var svg = d3
      .select(PieChartRef.current)
      .append('svg')
      .attr('viewBox', `0 0 ` + width + ' ' + height)
      .append('g')
      .attr('transform', 'translate(' + width / 2 + ',' + height / 2 + ')')

    var root = d3.hierarchy(props.root)
    root.sum(function (d) {
      return d.size
    })

    svg
      .selectAll('path')
      .data(partition(root).descendants())
      .enter()
      .append('path')
      .attr('d', arc)
      .style('fill', function (d) {
        return (d.children ? d : d.parent).data.color
      })
      .attr('stroke', '#141721')
      .style('stroke-width', '1px')
      .on('click', click)
      .append('title')
      .text(function (d) {
        return d.data.name + '\n' + formatNumber(d.value)
      })

    function click(d) {
      svg
        .transition()
        .duration(750)
        .tween('scale', function () {
          var xd = d3.interpolate(x.domain(), [d.x0, d.x1]),
            yd = d3.interpolate(y.domain(), [d.y0, 1]),
            yr = d3.interpolate(y.range(), [d.y0 ? 20 : 0, radius])
          return function (t) {
            x.domain(xd(t))
            y.domain(yd(t)).range(yr(t))
          }
        })
        .selectAll('path')
        .attrTween('d', function (d) {
          return function () {
            return arc(d)
          }
        })
    }
  }, [])

  return (
    <div>
      <div ref={PieChartRef}></div>
    </div>
  )
}

export default PieChart
