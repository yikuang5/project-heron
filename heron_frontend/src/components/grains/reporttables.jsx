import { React, useState, useEffect } from 'react'
import axios from 'axios'
import JSSoup from 'jssoup'
import { Link } from 'react-router-dom'
import Box from '@material-ui/core/Box'
import Collapse from '@material-ui/core/Collapse'
import IconButton from '@material-ui/core/IconButton'
import Table from '@material-ui/core/Table'
import TableBody from '@material-ui/core/TableBody'
import TableCell from '@material-ui/core/TableCell'
import TableContainer from '@material-ui/core/TableContainer'
import TableHead from '@material-ui/core/TableHead'
import TableRow from '@material-ui/core/TableRow'
import Typography from '@material-ui/core/Typography'
import Paper from '@material-ui/core/Paper'
import KeyboardArrowDownIcon from '@material-ui/icons/KeyboardArrowDown'
import KeyboardArrowUpIcon from '@material-ui/icons/KeyboardArrowUp'

import reporttableStyles from '../../styles/grainStyles/reporttables.module.css'

function Row(props) {
  const { row } = props
  const [open, setOpen] = useState(false)

  return (
    <>
      <TableRow className={reporttableStyles.tableheadsucell}>
        <TableCell className={reporttableStyles.tableheadsucell}>
          <IconButton aria-label="expand row" size="small" onClick={() => setOpen(!open)}>
            {open ? <KeyboardArrowUpIcon /> : <KeyboardArrowDownIcon />}
          </IconButton>
        </TableCell>

        <TableCell component="th" scope="row" className={reporttableStyles.tableheadsucell}>
          {row.name}
        </TableCell>

        <TableCell align="right" className={reporttableStyles.tableheadsucell}>
          {row.freq}
        </TableCell>

        <TableCell align="right" className={reporttableStyles.tableheadsucellrelease}>
          {row.nextrelease}
        </TableCell>
      </TableRow>

      <TableRow>
        <TableCell style={{ paddingBottom: 0, paddingTop: 0 }} colSpan={6}>
          <Collapse in={open} timeout="auto" unmountOnExit>
            <Box margin={1}>
              <Typography
                variant="h7"
                gutterBottom
                component="div"
                className={reporttableStyles.headertitle}
              >
                {row.name}
              </Typography>
              <p className={reporttableStyles.namedesc}> {row.namedesc} </p>

              <Table size="small" aria-label="purchases">
                <TableHead className={reporttableStyles.tablehead}>
                  <TableRow>
                    <TableCell className={reporttableStyles.tableheadsubcell0}>
                      Release Date
                    </TableCell>
                    <TableCell className={reporttableStyles.tableheadsubcell1}>
                      Link to Report
                    </TableCell>
                  </TableRow>
                </TableHead>

                <TableBody className={reporttableStyles.tablebody}>
                  {row.history.map(historyRow => (
                    <TableRow key={historyRow.date} className={reporttableStyles.tablerow}>
                      <TableCell
                        component="th"
                        scope="row"
                        className={reporttableStyles.tableheadcontentcell}
                      >
                        {historyRow.arialabel}
                      </TableCell>

                      <TableCell className={reporttableStyles.tableheadcontentcell}>
                        <Link
                          className={reporttableStyles.urlLinks}
                          to={{ pathname: `${historyRow.txt_url}` }}
                          target="_blank"
                        >
                          {' '}
                          PDF{' '}
                        </Link>
                      </TableCell>
                    </TableRow>
                  ))}
                </TableBody>
              </Table>
            </Box>
          </Collapse>
        </TableCell>
      </TableRow>
    </>
  )
}

export default function CollapsibleTable() {
  const [reportdata, setreportdata] = useState()
  const [wasdedata, setwasdedata] = useState()
  const [feeddata, setfeeddata] = useState()

  const [reportloading, setreportloading] = useState(true)
  const [wasdeloading, setwasdeloading] = useState(true)
  const [feedloading, setfeedloading] = useState(true)

  function onlyUnique(value, index, self) {
    return self.indexOf(value) === index
  }
  function createData(name, nextrelease, namedesc, freq, cropProduction) {
    return {
      name,
      nextrelease,
      namedesc,
      freq,
      history: cropProduction
    }
  }
  function ESRScraper(url, reportTitle, reportDesc, reportFreq, statefuldata, loadstatus) {
    axios.get(url).then(response => {
      var arrItems = new JSSoup(response.data).findAll('a', 'btn')
      var arrUrls = arrItems
        .map(x => {
          return [x.attrs.href, x.attrs['aria-label'].split('-')[1]]
        })
        .filter(onlyUnique)
      var arrLength = arrUrls.length

      try {
        var nextReleases = [].concat.apply(
          [],
          new JSSoup(response.data).findAll('span', 'upcoming_releases').map(x => {
            return x.contents.map(x => {
              return x._text
            })
          })
        )
      } catch {}
      if (nextReleases.length !== 0) {
        var nextReleaseDate = nextReleases[0].slice(0, 6) + nextReleases[0].slice(-9)
      } else {
        var nextReleaseDate = 'Not Given'
      }

      var cropProduction = []
      for (var i = 0; i < arrLength; i++) {
        if (arrUrls[i][0].slice(-4) === '.pdf') {
          cropProduction.push({ arialabel: arrUrls[i][1], txt_url: arrUrls[i][0] })
        }
      }

      var p = [createData(reportTitle, nextReleaseDate, reportDesc, reportFreq, cropProduction)]
      statefuldata(p)
      loadstatus(false)

      return nextReleaseDate, cropProduction
    })
  }

  useEffect(() => {
    ESRScraper(
      'https://usda.library.cornell.edu/concern/publications/tm70mv177?locale=en&page=1#release-items',
      'Crop Production',
      'This monthly report contains crop production data for the U.S., including acreage, area harvested, and yield. Wheat, fruits, nuts, and hops are the specific crops included in the report, but data on planted and harvested crop area are also included. There is also a strong focus on maple syrup, as the report details production, value, season dates, and percent of sales by type. The report also contains a monthly weather summary, a monthly agricultural summary, and an analysis of precipitation and the degree of departure from the normal precipitation map for the month.',
      'Monthly',
      setreportdata,
      setreportloading
    )
    ESRScraper(
      'https://usda.library.cornell.edu/concern/publications/3t945q76s?locale=en#release-items',
      'WASDE',
      'This monthly report provides the current USDA forecasts of U.S. and world supply-use balances of major grains, soybeans and products, and cotton; and U.S. supply and use of sugar and livestock products.',
      'Monthly',
      setwasdedata,
      setwasdeloading
    )
    ESRScraper(
      'https://usda.library.cornell.edu/concern/publications/44558d29f?locale=en',
      'Feed Outlook',
      'This publication examines the season-average price, production, supply, use, and trade for feed grains t in major exporting and importing countries. The report focuses on corn, but covers sorghum, barley, oats, hay, and wheat as well. Contains future projections production, supply, use, and trade made based on weather conditions and current agricultural and industrial developments',
      'Monthly',
      setfeeddata,
      setfeedloading
    )
  }, [])

  if (wasdeloading || reportloading || feedloading) {
    return <div> </div>
  } else {
    return (
      <div className={reporttableStyles.tablediv}>
        <TableContainer component={Paper} className={reporttableStyles.tablecontainer}>
          <Table aria-label="collapsible table" stickyHeader>
            <TableHead className={reporttableStyles.tablehead}>
              <TableRow>
                <TableCell className={reporttableStyles.tableheadcell0} />
                <TableCell className={reporttableStyles.tableheadcell1}> Report </TableCell>
                <TableCell align="right" className={reporttableStyles.tableheadcell2}>
                  {' '}
                  Frequency{' '}
                </TableCell>
                <TableCell align="right" className={reporttableStyles.tableheadcell3}>
                  {' '}
                  Next Release{' '}
                </TableCell>
              </TableRow>
            </TableHead>
            <TableBody>
              {reportdata.map(row => (
                <Row key={row.name} row={row} />
              ))}
              {wasdedata.map(row => (
                <Row key={row.name} row={row} />
              ))}
              {feeddata.map(row => (
                <Row key={row.name} row={row} />
              ))}
            </TableBody>
          </Table>
        </TableContainer>
      </div>
    )
  }
}
