import { React, useState, useEffect } from 'react'
import axios from 'axios'
import { AreaChart, Area, XAxis, YAxis, ReferenceArea, Tooltip, Line, LineChart } from 'recharts'
import pcStyles from '../../styles/grainStyles/progresscondition.module.css'

export default function USLineSeriesArea() {
  const width = 700
  const height = 200

  const [progressdata, setprogressdata] = useState(null)
  const [conditiondata, setconditiondata] = useState(null)

  useEffect(() => {
    axios.get('api/nassquickstats').then(response => {
      var conditionitem = JSON.parse(JSON.parse(response.data)['CONDITION'])
      var progressitem = JSON.parse(JSON.parse(response.data)['PROGRESS'])

      function updateCharts(year, calculation_type) {
        var cleanedCondition = []
        var cleanedProgress = []
        for (let i = 0; i < Object.keys(conditionitem).length; i++) {
          if (
            conditionitem[i].YEAR === year &&
            conditionitem[i].CALCULATION_TYPE === calculation_type &&
            conditionitem[i].WEEK < 53
          ) {
            cleanedCondition.push(conditionitem[i])
          }
        }
        for (let i = 0; i < Object.keys(progressitem).length; i++) {
          if (
            progressitem[i].YEAR === year &&
            progressitem[i].CALCULATION_TYPE === calculation_type &&
            progressitem[i].WEEK < 53
          ) {
            cleanedProgress.push(progressitem[i])
          }
        }
        setconditiondata(cleanedCondition)
        setprogressdata(cleanedProgress)
      }

      updateCharts(2021, 'mean')
    })
  }, [])

  return (
    <>
      <div className={pcStyles.areaChart}>
        <AreaChart
          width={width}
          height={height}
          data={conditiondata}
          margin={{ top: 5, right: 30, left: 20, bottom: 5 }}
        >
          <XAxis
            dataKey="WEEK"
            type="number"
            domain={[0, 52]}
            tickCount={14}
            tick={{ fontSize: 11 }}
          />
          <YAxis type="number" domain={[0, 100]} allowDataOverflow={true} tick={{ fontSize: 11 }} />
          <Tooltip />
          <Area type="monotone" dataKey="PCT_EXCELLENT" stackId="1" stroke="black" fill="#318728" />
          <Area type="monotone" dataKey="PCT_GOOD" stackId="1" stroke="black" fill="#3FAB33" />
          <Area type="monotone" dataKey="PCT_FAIR" stackId="1" stroke="black" fill="#53C846" />
          <Area type="monotone" dataKey="PCT_POOR" stackId="1" stroke="black" fill="#75D36B" />
          <Area type="monotone" dataKey="PCT_VERY_POOR" stackId="1" stroke="black" fill="#DEFF91" />
          <ReferenceArea x1={13} x2={21} fill="#018515" />
          <ReferenceArea x1={34} x2={47} fill="#ff8c04" />
        </AreaChart>
      </div>
      <div>
        <LineChart
          width={width}
          height={height}
          data={progressdata}
          margin={{ top: 5, right: 30, left: 20, bottom: 5 }}
        >
          <XAxis
            dataKey="WEEK"
            type="number"
            domain={[0, 52]}
            tickCount={14}
            tick={{ fontSize: 11 }}
          />
          <YAxis type="number" domain={[0, 100]} tick={{ fontSize: 11 }} />
          <Tooltip />
          <Line type="monotone" dataKey="PCT_DENTED" stroke="#00f0e8" strokeWidth={2} dot={false} />
          <Line type="monotone" dataKey="PCT_DOUGH" stroke="#0003b8" strokeWidth={2} dot={false} />
          <Line
            type="monotone"
            dataKey="PCT_EMERGED"
            stroke="#04ba13"
            strokeWidth={2}
            dot={false}
          />
          <Line
            type="monotone"
            dataKey="PCT_HARVESTED"
            stroke="#ff8c04"
            strokeWidth={2}
            dot={false}
          />
          <Line type="monotone" dataKey="PCT_MATURE" stroke="#611d00" strokeWidth={2} dot={false} />
          <Line type="monotone" dataKey="PCT_MILK" stroke="#b0b0b0" strokeWidth={2} dot={false} />
          <Line
            type="monotone"
            dataKey="PCT_PLANTED"
            stroke="#018515"
            strokeWidth={2}
            dot={false}
          />
          <Line
            type="monotone"
            dataKey="PCT_SEEDBED_PREPARED"
            stroke="#e30013"
            strokeWidth={2}
            dot={false}
          />
          <Line
            type="monotone"
            dataKey="PCT_SILKING"
            stroke="#bd06aa"
            strokeWidth={2}
            dot={false}
          />
        </LineChart>
      </div>
    </>
  )
}
