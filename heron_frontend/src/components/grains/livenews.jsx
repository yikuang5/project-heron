import { React, useState, useEffect } from 'react'
import axios from 'axios'
import { Link } from 'react-router-dom'

import livenewsStyles from '../../styles/grainStyles/livenews.module.css'

import { makeStyles } from '@material-ui/core/styles'
import Box from '@material-ui/core/Box'
import Collapse from '@material-ui/core/Collapse'
import IconButton from '@material-ui/core/IconButton'
import Table from '@material-ui/core/Table'
import TableBody from '@material-ui/core/TableBody'
import TableCell from '@material-ui/core/TableCell'
import TableContainer from '@material-ui/core/TableContainer'
import TableHead from '@material-ui/core/TableHead'
import TableRow from '@material-ui/core/TableRow'
import Paper from '@material-ui/core/Paper'
import KeyboardArrowDownIcon from '@material-ui/icons/KeyboardArrowDown'
import KeyboardArrowUpIcon from '@material-ui/icons/KeyboardArrowUp'

const useRowStyles = makeStyles({
  root: {
    fontSize: '30pt',
    '& > *': {
      borderBottom: 'unset'
    }
  }
})

function Row(props) {
  const { row } = props
  const [open, setOpen] = useState(false)
  const classes = useRowStyles()

  function toggleContent(content) {
    if (content === 'nan') {
      return ''
    } else {
      return content
    }
  }

  const months = [
    'Jan',
    'Feb',
    'Mar',
    'Apr',
    'May',
    'Jun',
    'Jul',
    'Aug',
    'Sep',
    'Oct',
    'Nov',
    'Dec'
  ]

  function toggleDate(date) {
    var formatteddate = new Date(date)
    formatteddate.setHours(formatteddate.getHours() - 8)
    var currentMonth = formatteddate.getMonth()
    return formatteddate.getDate() + ' ' + months[currentMonth] + ' ' + date.slice(11, 16)
  }

  return (
    <>
      <TableRow className={classes.root}>
        <TableCell className={livenewsStyles.tablecell0}>
          <IconButton aria-label="expand row" size="small" onClick={() => setOpen(!open)}>
            {open ? <KeyboardArrowUpIcon /> : <KeyboardArrowDownIcon />}
          </IconButton>
        </TableCell>
        <TableCell className={livenewsStyles.tablecell1} component="th" scope="row">
          {row.TITLE}
        </TableCell>
        <TableCell className={livenewsStyles.tablecell2} align="right">
          {toggleDate(row.SGT_DATEFIELD)}
        </TableCell>
      </TableRow>
      <TableRow>
        <TableCell style={{ paddingBottom: 0, paddingTop: 0 }} colSpan={6}>
          <Collapse in={open} timeout="auto" unmountOnExit>
            <Box margin={1}>
              <h3 className={livenewsStyles.newsHeader}> {row.TITLE} </h3>
              <span className={livenewsStyles.authorsource}>
                {' '}
                <b> Author: </b> {row.AUTHOR}{' '}
              </span>{' '}
              <span className={livenewsStyles.authorsource}>
                {' '}
                <b> Source: </b> {row.SOURCE}{' '}
              </span>
              <p className={livenewsStyles.description}> {row.DESCRIPTION} </p>
              <p className={livenewsStyles.content}> {toggleContent(row.CONTENT)} </p>
              <p>
                {' '}
                <Link
                  className={livenewsStyles.urlLinks}
                  to={{ pathname: `${row.URL}` }}
                  target="_blank"
                >
                  {' '}
                  READ MORE HERE{' '}
                </Link>{' '}
              </p>
            </Box>
          </Collapse>
        </TableCell>
      </TableRow>
    </>
  )
}

export default function CollapsibleTable() {
  const [newsdata, setnewsdata] = useState()
  const [loading, setloading] = useState(true)

  function updateTable() {
    axios.get('api/globalNews/').then(function (data) {
      // Retrieve the values from the news database
      setnewsdata(
        JSON.parse(data.data)
          .reverse()
          .map((x, i) => {
            return x.fields
          })
      )
      setloading(false)
    })
  }

  useEffect(() => {
    updateTable()
    const interval = setInterval(() => {
      updateTable()
    }, 40000)
    return () => clearInterval(interval)
  }, [])

  if (loading) {
    return (
      <div className={livenewsStyles.mainDiv}>
        <TableContainer component={Paper} className={livenewsStyles.tablecontainer}>
          <Table aria-label="collapsible table" stickyHeader className={livenewsStyles.table}>
            <TableHead className={livenewsStyles.tablehead}>
              <TableRow>
                <TableCell className={livenewsStyles.tableheadcell0} />
                <TableCell className={livenewsStyles.tableheadcell1}> Title </TableCell>
                <TableCell align="right" className={livenewsStyles.tableheadcell2}>
                  {' '}
                  Time{' '}
                </TableCell>
              </TableRow>
            </TableHead>
            <TableBody> </TableBody>
          </Table>
        </TableContainer>
      </div>
    )
  } else {
    return (
      <div className={livenewsStyles.mainDiv}>
        <TableContainer component={Paper} className={livenewsStyles.tablecontainer}>
          <Table aria-label="collapsible table" stickyHeader className={livenewsStyles.table}>
            <TableHead className={livenewsStyles.tablehead}>
              <TableRow>
                <TableCell className={livenewsStyles.tableheadcell0} />
                <TableCell className={livenewsStyles.tableheadcell1}> Title </TableCell>
                <TableCell align="right" className={livenewsStyles.tableheadcell2}>
                  {' '}
                  Time{' '}
                </TableCell>
              </TableRow>
            </TableHead>
            <TableBody>
              {newsdata.map(row => (
                <Row key={row.TITLE} row={row} />
              ))}
            </TableBody>
          </Table>
        </TableContainer>
      </div>
    )
  }
}
