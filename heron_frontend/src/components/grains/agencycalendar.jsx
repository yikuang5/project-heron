import { React, useState, useEffect } from 'react'
import axios from 'axios'
import { Link } from 'react-router-dom'

import { makeStyles } from '@material-ui/core/styles'

import Table from '@material-ui/core/Table'
import TableBody from '@material-ui/core/TableBody'
import TableCell from '@material-ui/core/TableCell'
import TableContainer from '@material-ui/core/TableContainer'
import TableHead from '@material-ui/core/TableHead'
import TableRow from '@material-ui/core/TableRow'
import Paper from '@material-ui/core/Paper'

import agencyCalendarStyles from '../../styles/grainStyles/agencycalendar.module.css'

const useRowStyles = makeStyles({
  root: {
    fontSize: '30pt',
    '& > *': {
      borderBottom: 'unset'
    }
  }
})

const months = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec']

function Row(props) {
  const classes = useRowStyles()
  const { row } = props
  var monthnumber

  function toggleDate(date, time) {
    if (date.slice(6) === 0) {
      monthnumber = date.slice(7, 7)
    } else {
      monthnumber = date.slice(5, 7)
    }
    return date.slice(8, 10) + ' ' + months[monthnumber - 1] + '   ' + time
  }

  var element = (
    <>
      <TableRow className={classes.root}>
        <TableCell className={agencyCalendarStyles.tablecellcontent} component="th" scope="row">
          {' '}
          <Link
            className={agencyCalendarStyles.urlLinks}
            to={{ pathname: `${row.link}` }}
            target="_blank"
          >
            {' '}
            {row.report}{' '}
          </Link>{' '}
        </TableCell>
        <TableCell className={agencyCalendarStyles.tablecellcontent} align="right">
          {row.agency}
        </TableCell>
        <TableCell className={agencyCalendarStyles.tablecellcontent} align="right">
          {toggleDate(row.release_date, row.time)}
        </TableCell>
      </TableRow>
    </>
  )

  return element
}

export default function AgencyCalendar() {
  const [agencydata, setagencydata] = useState()
  const [loading, setloading] = useState(true)
  var jsondata = []
  function updateAgencyReports() {
    axios.get('api/agencyreports').then(function (data) {
      var json_arr = JSON.parse(data.data)
      for (let i = 0; i < Object.keys(json_arr).length; i++) {
        jsondata.push(json_arr[i])
      }
      setagencydata(jsondata)
      setloading(false)
    })
  }

  useEffect(() => {
    updateAgencyReports()
    const interval = setInterval(() => {
      updateAgencyReports()
    }, 600000)
    return () => clearInterval(interval)
  }, [])

  if (loading) {
    return (
      <TableContainer component={Paper} className={agencyCalendarStyles.tableContainer}>
        <Table aria-label="collapsible table" stickyHeader>
          <TableHead>
            <TableRow>
              <TableCell className={agencyCalendarStyles.tablecellheader}> Report </TableCell>
              <TableCell className={agencyCalendarStyles.tablecellheader}> Agency </TableCell>
              <TableCell className={agencyCalendarStyles.tablecellheader}> Release Date </TableCell>
            </TableRow>
          </TableHead>
          <TableBody> </TableBody>
        </Table>
      </TableContainer>
    )
  } else {
    return (
      <TableContainer component={Paper} className={agencyCalendarStyles.tableContainer}>
        <Table aria-label="collapsible table" stickyHeader>
          <TableHead>
            <TableRow>
              <TableCell className={agencyCalendarStyles.tablecellheader0}> Report </TableCell>
              <TableCell className={agencyCalendarStyles.tablecellheader1}> Agency </TableCell>
              <TableCell className={agencyCalendarStyles.tablecellheader2}>
                {' '}
                Release Date{' '}
              </TableCell>
            </TableRow>
          </TableHead>
          <TableBody>
            {agencydata.map(row => (
              <Row key={row.report} row={row} />
            ))}
          </TableBody>
        </Table>
      </TableContainer>
    )
  }
}
