import React from 'react'
import axios from 'axios'

import PortfolioAdditorStyles from '../../styles/accountStyles/portfolioadditor.module.css'

import Table from '@material-ui/core/Table'
import TableBody from '@material-ui/core/TableBody'
import TableCell from '@material-ui/core/TableCell'
import TableContainer from '@material-ui/core/TableContainer'
import TableHead from '@material-ui/core/TableHead'
import TableRow from '@material-ui/core/TableRow'
import IconButton from '@material-ui/core/IconButton'
import Paper from '@material-ui/core/Paper'
import { StylesProvider } from '@material-ui/core/styles'

import AddIcon from '@material-ui/icons/Add'
import CloseIcon from '@material-ui/icons/Close'

import Popup from 'reactjs-popup'

export const baseUrl = 'http://localhost:8000/api'
export const headers = {
  'Content-type': 'application/json'
}

class portfolioTable extends React.Component {
  constructor(props) {
    super(props)
    this.state = {
      items: [],
      OrderId: '',
      Symbol: '',
      Shares: '',
      EquiValue: '',
      ComissionFees: '',
      ExecutionPrice: '',
      ExecutionDate: '',
      Position: '',
      FinancialInstrument: '',
      Currency: '',
      Broker: '',
      registered: 'REGISTERED' // Set State to see if component is registered in database or not
    }
    this.changeHandler = this.changeHandler.bind(this)
    this.AddElementsOnSubmit = this.AddElementsOnSubmit.bind(this)
  }

  changeHandler(event) {
    this.setState({ [event.target.name]: event.target.value })
  }

  AddElementsOnSubmit() {
    const uid = function () {
      return Date.now().toString(36) + Math.random().toString(36)
    }

    var OrderId = this.state.OrderId
    var Symbol = this.state.Symbol
    var Shares = this.state.Shares
    var ComissionFees = this.state.ComissionFees
    var ExecutionPrice = this.state.ExecutionPrice
    var ExecutionDate = this.state.ExecutionDate
    var Position = this.state.Position
    var EquiValue = this.state.EquiValue
    var FinancialInstrument = this.state.FinancialInstrument
    var Currency = this.state.Currency
    var Broker = this.state.Broker
    var elements = this.state.items.slice()
    var Registered = this.state.Registered

    var newrow = {
      OrderId: OrderId,
      Symbol: Symbol,
      Shares: Shares,
      EquiValue: EquiValue,
      ComissionFees: ComissionFees,
      ExecutionPrice: ExecutionPrice,
      ExecutionDate: ExecutionDate,
      Position: Position,
      FinancialInstrument: FinancialInstrument,
      Currency: Currency,
      Broker: Broker,
      Registered: Registered
    }

    this.setState({
      items: elements,
      OrderId: '',
      Symbol: '',
      Shares: '',
      EquiValue: '',
      ComissionFees: '',
      ExecutionPrice: '',
      ExecutionDate: '',
      Position: '',
      FinancialInstrument: '',
      Currency: '',
      Broker: '',
      Registered: 'REGISTERED'
    })

    var genOrderId = uid()
    // Provide cross checking functionalities. If symbol is already in your portfolio, update the values so as to not create duplicate symbol entries.
    axios
      .post(baseUrl + '/portfolio', {
        OrderId: genOrderId,
        Symbol: Symbol,
        Shares: Shares,
        EquiValue: EquiValue,
        ComissionFees: ComissionFees,
        ExecutionPrice: ExecutionPrice,
        ExecutionDate: ExecutionDate,
        Position: Position,
        FinancialInstrument: FinancialInstrument,
        Currency: Currency,
        Broker: Broker
      })
      .then(response => {
        newrow['Registered'] = 'REGISTERED'
        newrow['OrderId'] = genOrderId
        console.log('Successfully added item into database ... ...')
      })
      .catch(error => {
        newrow['Registered'] = 'NOT REGISTERED'
        console.log(error)
      })
    console.log(newrow)
    elements.push(newrow)
  }

  deleteItem(i) {
    const { items } = this.state
    // Delete the entry from the database altogether
    axios
      .delete(baseUrl + '/portfolio/' + items[i]['OrderId'], {})
      .then(function (response) {
        console.log('Successfully deleted item from database ... ...')
      })
      .catch(function (error) {
        console.log(error)
      })
    items.splice(i, 1)
    this.setState({ items })
  }

  componentDidMount() {
    axios
      .get(baseUrl + '/portfolio')
      .then(response => {
        this.setState({
          items: response.data.map(function (res) {
            var o = Object.assign({}, res)
            o.Registered = 'REGISTERED'
            return o
          })
        })
      })
      .catch(e => {
        console.warn('Error while getting portfolio details ... ...')
      })
  }

  render() {
    return (
      <>
        <div className={PortfolioAdditorStyles.portfolioBox}>
          <StylesProvider injectFirst>
            <TableContainer component={Paper}>
              <Table
                aria-label="collapsible table"
                stickyHeader
                className={PortfolioAdditorStyles.tickerPortfolioTable}
              >
                <TableHead>
                  <TableRow key={'headers'}>
                    <TableCell className={PortfolioAdditorStyles.tickerPortfolioHead}>
                      {' '}
                      Order ID{' '}
                    </TableCell>
                    <TableCell className={PortfolioAdditorStyles.tickerPortfolioHead}>
                      {' '}
                      Symbol{' '}
                    </TableCell>
                    <TableCell className={PortfolioAdditorStyles.tickerPortfolioHead}>
                      {' '}
                      Shares{' '}
                    </TableCell>
                    <TableCell className={PortfolioAdditorStyles.tickerPortfolioHead}>
                      {' '}
                      Equivalent Value{' '}
                    </TableCell>
                    <TableCell className={PortfolioAdditorStyles.tickerPortfolioHead}>
                      {' '}
                      Position{' '}
                    </TableCell>
                    <TableCell className={PortfolioAdditorStyles.tickerPortfolioHead}>
                      {' '}
                      Execution Price{' '}
                    </TableCell>
                    <TableCell className={PortfolioAdditorStyles.tickerPortfolioHead}>
                      {' '}
                      Execution Date{' '}
                    </TableCell>
                    <TableCell className={PortfolioAdditorStyles.tickerPortfolioHead}>
                      {' '}
                      Financial Instrument{' '}
                    </TableCell>
                    <TableCell className={PortfolioAdditorStyles.tickerPortfolioHead}>
                      {' '}
                      Currency{' '}
                    </TableCell>
                    <TableCell className={PortfolioAdditorStyles.tickerPortfolioHead}>
                      {' '}
                      Broker{' '}
                    </TableCell>
                    <TableCell className={PortfolioAdditorStyles.tickerPortfolioHead}>
                      {' '}
                      Registered{' '}
                    </TableCell>
                    <TableCell className={PortfolioAdditorStyles.tickerPortfolioHead}>
                      {' '}
                      Remove Entry{' '}
                    </TableCell>
                    <TableCell className={PortfolioAdditorStyles.tickerPortfolioHead}>
                      <Popup
                        trigger={
                          <IconButton
                            aria-label="close"
                            className={PortfolioAdditorStyles.AddButton}
                          >
                            {' '}
                            <AddIcon style={{ fontSize: 15 }} />{' '}
                          </IconButton>
                        }
                        modal
                      >
                        {close => (
                          <div className={PortfolioAdditorStyles.dialogue}>
                            <IconButton
                              className={PortfolioAdditorStyles.closeButton}
                              color="secondary"
                              aria-label="add row"
                              onClick={() => {
                                close()
                              }}
                            >
                              {' '}
                              <CloseIcon />{' '}
                            </IconButton>
                            <h4 className={PortfolioAdditorStyles.formtitle}>
                              {' '}
                              Account Purchases{' '}
                            </h4>
                            <div> Symbol </div>
                            <div>
                              {' '}
                              <input
                                type="text"
                                placeholder="Symbol"
                                autoComplete="off"
                                onChange={this.changeHandler}
                                label="Symbol"
                                name="Symbol"
                                value={this.state.symbol}
                              />{' '}
                            </div>
                            <div> Shares </div>
                            <div>
                              {' '}
                              <input
                                type="text"
                                placeholder="No. of Shares"
                                autoComplete="off"
                                onChange={this.changeHandler}
                                label="Shares"
                                name="Shares"
                                value={this.state.shares}
                              />{' '}
                            </div>
                            <div> Equivalent Value </div>
                            <div>
                              {' '}
                              <input
                                type="text"
                                placeholder="Equiv Value"
                                autoComplete="off"
                                onChange={this.changeHandler}
                                label="EquiValue"
                                name="EquiValue"
                                value={this.state.EquiValue}
                              />{' '}
                            </div>
                            <div> Comission Fees </div>
                            <div>
                              {' '}
                              <input
                                type="text"
                                placeholder="$0.00"
                                autoComplete="off"
                                onChange={this.changeHandler}
                                label="ComissionFees"
                                name="ComissionFees"
                                value={this.state.comission}
                              />{' '}
                            </div>
                            <div> Price </div>
                            <div>
                              {' '}
                              <input
                                type="text"
                                placeholder="Price"
                                autoComplete="off"
                                onChange={this.changeHandler}
                                label="ExecutionPrice"
                                name="ExecutionPrice"
                                value={this.state.price}
                              />{' '}
                            </div>
                            <div> Date Executed </div>
                            <div>
                              {' '}
                              <input
                                type="text"
                                placeholder="YYYY-MM-DD"
                                autoComplete="off"
                                onChange={this.changeHandler}
                                label="ExecutionDate"
                                name="ExecutionDate"
                                value={this.state.ex_date}
                              />{' '}
                            </div>
                            <div> Position </div>
                            <div>
                              {' '}
                              <input
                                type="text"
                                placeholder="Long/Short"
                                autoComplete="off"
                                onChange={this.changeHandler}
                                label="Position"
                                name="Position"
                                value={this.state.longshort}
                              />{' '}
                            </div>
                            <div> Financial Instrument </div>
                            <div>
                              {' '}
                              <input
                                type="text"
                                placeholder="Financial Instrument"
                                autoComplete="off"
                                onChange={this.changeHandler}
                                label="FinancialInstrument"
                                name="FinancialInstrument"
                                value={this.state.financialinstrument}
                              />{' '}
                            </div>
                            <div> Currency </div>
                            <div>
                              {' '}
                              <input
                                type="text"
                                placeholder="Currency"
                                autoComplete="off"
                                onChange={this.changeHandler}
                                label="Currency"
                                name="Currency"
                                value={this.state.Currency}
                              />{' '}
                            </div>
                            <div> Broker </div>
                            <div>
                              {' '}
                              <input
                                type="text"
                                placeholder="Broker"
                                autoComplete="off"
                                onChange={this.changeHandler}
                                label="Broker"
                                name="Broker"
                                value={this.state.Broker}
                              />{' '}
                            </div>
                            <input
                              className={PortfolioAdditorStyles.submitButton}
                              type="submit"
                              value="submit"
                              onClick={() => this.AddElementsOnSubmit()}
                            />
                          </div>
                        )}
                      </Popup>
                    </TableCell>
                  </TableRow>
                </TableHead>
                <TableBody>
                  {this.state.items.map((row, i) => (
                    <TableRow key={`row-${i}`}>
                      <TableCell className={PortfolioAdditorStyles.tickerPortfolioCell}>
                        {' '}
                        {row.OrderId}{' '}
                      </TableCell>
                      <TableCell className={PortfolioAdditorStyles.tickerPortfolioCell}>
                        {' '}
                        {row.Symbol}{' '}
                      </TableCell>
                      <TableCell className={PortfolioAdditorStyles.tickerPortfolioCell}>
                        {' '}
                        {row.Shares}{' '}
                      </TableCell>
                      <TableCell className={PortfolioAdditorStyles.tickerPortfolioCell}>
                        {' '}
                        {row.EquiValue}{' '}
                      </TableCell>
                      <TableCell className={PortfolioAdditorStyles.tickerPortfolioCell}>
                        {' '}
                        {row.Position}{' '}
                      </TableCell>
                      <TableCell className={PortfolioAdditorStyles.tickerPortfolioCell}>
                        {' '}
                        {row.ExecutionPrice}{' '}
                      </TableCell>
                      <TableCell className={PortfolioAdditorStyles.tickerPortfolioCell}>
                        {' '}
                        {row.ExecutionDate}{' '}
                      </TableCell>
                      <TableCell className={PortfolioAdditorStyles.tickerPortfolioCell}>
                        {' '}
                        {row.FinancialInstrument}{' '}
                      </TableCell>
                      <TableCell className={PortfolioAdditorStyles.tickerPortfolioCell}>
                        {' '}
                        {row.Currency}{' '}
                      </TableCell>
                      <TableCell className={PortfolioAdditorStyles.tickerPortfolioCell}>
                        {' '}
                        {row.Broker}{' '}
                      </TableCell>
                      <TableCell
                        className={
                          row.Registered === 'NOT REGISTERED'
                            ? PortfolioAdditorStyles.unreg
                            : PortfolioAdditorStyles.reg
                        }
                      >
                        {' '}
                        {row.Registered}{' '}
                      </TableCell>
                      <TableCell className={PortfolioAdditorStyles.tickerPortfolioCell}>
                        {' '}
                        <IconButton
                          onClick={this.deleteItem.bind(this, i)}
                          color="secondary"
                          className={PortfolioAdditorStyles.deleteButton}
                        >
                          {' '}
                          <CloseIcon style={{ fontSize: 15 }} />{' '}
                        </IconButton>{' '}
                      </TableCell>
                    </TableRow>
                  ))}
                </TableBody>
              </Table>
            </TableContainer>
          </StylesProvider>
        </div>
      </>
    )
  }
}

export default portfolioTable
